<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddRequest;
use Illuminate\Http\Response;
use App\ProxyData;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Redis;
use League\OAuth2\Server\RequestEvent;
use Illuminate\Support\Facades\Auth; 


class UserController extends Controller
{
    //

    public $successStatus = 200;
    public $failedStatus = 404;


    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['name'] =  $user->name;
            $success['email'] = $user->email;
            $success['user_id'] = $user->id;
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function register(Request $request) 
    { 


        $input = $request->all();
        $rules = array('email' => 'unique:users,email');
        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            $success['status'] = "404";
            $success['message'] = "This email already exists";
            return response()->json(['success'=> $success]); 
        }

      
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['status'] = "200";
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;
        $success['email'] = $user->email;
        

         return response()->json(['success'=>$success]); 
    }

    public function details() {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }


    public function proxies (Request $request) {
        $input = $request->all();
        $rules = array('user_id' => 'unique:users,id');
        $validator = Validator::make($input, $rules);
           if($validator->fails()) {
                $user = ProxyData::create($input);
                $success['status'] = "200";
                $success['name'] = $user->name;
                $success['user_id'] = $user->id;
                return response()->json(['success' => $user], $this->successStatus);
           }

            else {
                $failed['message'] = "User Not Exists";

                return response()->json(['success'=> $failed]); 

            }
     
     

    }

    public function getAllproxies () {
        
        return response(ProxyData::orderBy('id', 'DESC')->get());

    }


    public function updateProxies(Request $request)
    {
        $row = ProxyData::find($request->input('user_id'));
        $row->fill($request->all());
        $row->save();
        return response()->json(['success' => $row], $this->successStatus);

    }
}
