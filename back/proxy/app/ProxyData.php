<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\User;

class ProxyData extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *  
     * @var array
     */
    protected $fillable = [
          'user_id',
          'proxy_id',
          'proxy_data',
          'proxy_data_expiry',
          'proxy_data_ip',
          'proxy_bind_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
}
