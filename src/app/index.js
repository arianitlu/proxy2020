import React, {Fragment} from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import Application from "./scenes/application";
import Auth from "./scenes/auth";

const App = ({history}) => {

    return (
      <Fragment>
          <Router history={history}>
              <Route path={`/`} component={Application} />
              <Route path="/(register|login)" component={Auth} />
          </Router>
      </Fragment>
  )
};

export default App;