import React, {useEffect} from "react";
import {NavbarVertical} from "../../components/NavbarVertical";
import {getProxyId, getUser, getUserId} from "../../helpers/web-storage-controller";
import 'react-coinbase-commerce/dist/coinbase-commerce-button.css';
import {requestCoinbaseHeaders} from "../../helpers/request-headers";

export const Price = () => {

    function setUserInvoiceFirst(mb,url, checkoutId) {

        const formDataa = new FormData();
        formDataa.append("user_id", getUserId());
        formDataa.append("proxy_id", getProxyId());
        formDataa.append("proxy_data", checkoutId);
        formDataa.append("package_name", getPackageBasedOnGB(mb));
        formDataa.append("package_amount", getPriceBasedOnMegabajt(mb));
        formDataa.append("completed_status", "Uncompleted");

        fetch(
            `https://rareproxy.io/back/public/api/proxies`,
            {
                method: "POST",
                headers: {},
                body: formDataa,
            }
        )
            .then((response) => response.json())
            .then((data) => {
                window.location.assign(url);
            });
    }

    function getPackageBasedOnGB(megabajt) {
        switch (megabajt) {
            case 1:
                return "Starter Pack";
                break;
            case 3:
                return "Ride Pack";
                break;
            case 5:
                return "Alpha Pack";
                break;
            case 10:
                return "Bravo Pack";
                break;
        }
    }

    function getPriceBasedOnMegabajt(megabajt) {
        switch (megabajt) {
            case 1:
                return "8$";
                break;
            case 3:
                return "23$";
                break;
            case 5:
                return "37$";
                break;
            case 10:
                return "70$";
                break;
        }
    }

    const handlePaketa1 = () => {
        const packet = {
            "name": "8.00",
            "description": "Mastering the Transition to the Information Age",
            "local_price": {
                "amount": "8.00",
                "currency": "USD"
            },
            "pricing_type": "fixed_price",
            "redirect_url": "https://rareproxy.io/dashboard",
            "cancel_url": "https://rareproxy.io/dashboard",
        };

        fetch(
            `https://api.commerce.coinbase.com/charges`,
            {
                method: "POST",
                headers: requestCoinbaseHeaders(),
                body: JSON.stringify(packet),
            }
        )
            .then((response) => response.json())
            .then((data) => {
                if (data && !!data.data) {
                    setUserInvoiceFirst(1,data.data.hosted_url,data.data.code);
                }
            });
    };

    const handlePaketa3 = () => {
        const packet = {
            "name": "23.00",
            "description": "Mastering the Transition to the Information Age",
            "local_price": {
                "amount": "23.00",
                "currency": "USD"
            },
            "pricing_type": "fixed_price",
            "redirect_url": "https://rareproxy.io/dashboard",
            "cancel_url": "https://rareproxy.io/dashboard",
        };

        fetch(
            `https://api.commerce.coinbase.com/charges`,
            {
                method: "POST",
                headers: requestCoinbaseHeaders(),
                body: JSON.stringify(packet),
            }
        )
            .then((response) => response.json())
            .then((data) => {
                if (data && !!data.data) {
                    setUserInvoiceFirst(3,data.data.hosted_url,data.data.code);
                }
            });
    };

    const handlePaketa5 = () => {
        const packet = {
            "name": "37.00",
            "description": "Mastering the Transition to the Information Age",
            "local_price": {
                "amount": "37.00",
                "currency": "USD"
            },
            "pricing_type": "fixed_price",
            "redirect_url": "https://rareproxy.io/dashboard",
            "cancel_url": "https://rareproxy.io/dashboard"
        };

        fetch(
            `https://api.commerce.coinbase.com/charges`,
            {
                method: "POST",
                headers: requestCoinbaseHeaders(),
                body: JSON.stringify(packet),
            }
        )
            .then((response) => response.json())
            .then((data) => {
                if (data && !!data.data) {
                    setUserInvoiceFirst(5,data.data.hosted_url,data.data.code);
                }
            });
    };

    // const handlePaketa7 = () => {
    //     const packet = {
    //         "name": "70.00",
    //         "description": "Mastering the Transition to the Information Age",
    //         "local_price": {
    //             "amount": "70.00",
    //             "currency": "USD"
    //         },
    //         "pricing_type": "fixed_price",
    //         "redirect_url": "https://rareproxy.io/dashboard",
    //         "cancel_url": "https://rareproxy.io/dashboard"
    //     };
    //
    //     fetch(
    //         `https://api.commerce.coinbase.com/charges`,
    //         {
    //             method: "POST",
    //             headers: requestCoinbaseHeaders(),
    //             body: JSON.stringify(packet),
    //         }
    //     )
    //         .then((response) => response.json())
    //         .then((data) => {
    //             if (data && !!data.data) {
    //                 setUserInvoiceFirst(7,data.data.hosted_url,data.data.code);
    //             }
    //         });
    // };

    const handlePaketa10 = () => {
        const packet = {
            "name": "70.00",
            "description": "Mastering the Transition to the Information Age",
            "local_price": {
                "amount": "70.00",
                "currency": "USD"
            },
            "pricing_type": "fixed_price",
            "redirect_url": "https://rareproxy.io/dashboard",
            "cancel_url": "https://rareproxy.io/dashboard"
        };

        fetch(
            `https://api.commerce.coinbase.com/charges`,
            {
                method: "POST",
                headers: requestCoinbaseHeaders(),
                body: JSON.stringify(packet),
            }
        )
            .then((response) => response.json())
            .then((data) => {
                if (data && !!data.data) {
                    setUserInvoiceFirst(10,data.data.hosted_url,data.data.code);
                }
            });
    };

    // const handlePaketa20 = () => {
    //     const packet = {
    //         "name": "200.00",
    //         "description": "Mastering the Transition to the Information Age",
    //         "local_price": {
    //             "amount": "200.00",
    //             "currency": "USD"
    //         },
    //         "pricing_type": "fixed_price",
    //         "redirect_url": "https://rareproxy.io/dashboard",
    //         "cancel_url": "https://rareproxy.io/dashboard"
    //     };
    //
    //     fetch(
    //         `https://api.commerce.coinbase.com/charges`,
    //         {
    //             method: "POST",
    //             headers: requestCoinbaseHeaders(),
    //             body: JSON.stringify(packet),
    //         }
    //     )
    //         .then((response) => response.json())
    //         .then((data) => {
    //             if (data && !!data.data) {
    //                 setUserInvoiceFirst(20,data.data.hosted_url,data.data.code);
    //             }
    //         });
    // };

    // const handlePaketa50 = () => {
    //     const packet = {
    //         "name": "490.00",
    //         "description": "Mastering the Transition to the Information Age",
    //         "local_price": {
    //             "amount": "490.00",
    //             "currency": "USD"
    //         },
    //         "pricing_type": "fixed_price",
    //         "redirect_url": "https://rareproxy.io/dashboard",
    //         "cancel_url": "https://rareproxy.io/dashboard"
    //     };
    //
    //     fetch(
    //         `https://api.commerce.coinbase.com/charges`,
    //         {
    //             method: "POST",
    //             headers: requestCoinbaseHeaders(),
    //             body: JSON.stringify(packet),
    //         }
    //     )
    //         .then((response) => response.json())
    //         .then((data) => {
    //             if (data && !!data.data) {
    //                 setUserInvoiceFirst(50,data.data.hosted_url,data.data.code);
    //             }
    //         });
    // };

    return (

        getUser() === null ? window.location.assign("/login")
            :
            (
                <>

                    <NavbarVertical/>
                    <div className="main-content" id="panel">
                        {/* Topnav */}
                        {/*<TopNav/>*/}
                        <nav className="navbar navbar-top navbar-expand" style={{background: '#54617e'}}>
                            <div className="container-fluid">
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    {/* Navbar links */}
                                    <ul className="navbar-nav align-items-center  ml-md-auto ">
                                        <li className="nav-item d-xl-none">
                                            {/* Sidenav toggler */}
                                            <div
                                                className="pr-3 sidenav-toggler sidenav-toggler-dark"
                                                data-action="sidenav-pin"
                                                data-target="#sidenav-main"
                                            >
                                                <div className="sidenav-toggler-inner">
                                                    <i className="sidenav-toggler-line"/>
                                                    <i className="sidenav-toggler-line"/>
                                                    <i className="sidenav-toggler-line"/>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>                        {/* Header */}
                        {/* Header */}
                        {/* Header */}
                        {/* Page content */}

                        <div className="header pb-4"
                             style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                        >
                            <div className="container-fluid">
                                <div className="header-body">
                                    <div className="row align-items-center py-4">
                                        <div className="col-lg-6 col-7"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="container-fluid mt--6">
                            <div className="row card-wrapper">
                                <div className="col-lg-6">
                                    <div className="card card-pricing border-0 text-center mb-4">
                                        <div className="card-header bg-transparent"><h4
                                            className="text-uppercase ls-1 py-3 mb-0"
                                            style={{textColor: '#53607e'}}

                                        >STARTER PACK</h4>
                                        </div>
                                        <div className="card-body px-lg-7">
                                            <div className="display-2">$8</div>
                                            <span className=" text-muted">per month</span>
                                            <ul className="list-unstyled my-4">
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-terminal"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">8$ per additional GB</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-pen-fancy"/></div>
                                                        </div>
                                                        <div><span
                                                            className="pl-2 text-sm">More than 150 countries</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-hdd"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">1GB data included</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <button type="button"
                                                    className="btn btn-primary mb-3"
                                                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                    onClick={handlePaketa1}
                                            >
                                                Purchase
                                            </button>
                                        </div>
                                        <div className="card-footer"><a
                                            href="file:///C:/Users/ASUS/Desktop/project/Project/price/Argon%20Dashboard%20PRO%20-%20Premium%20Bootstrap%204%20Admin%20Template.htm"
                                            className=" text-muted">Unlimited threads</a></div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="card card-pricing border-0 text-center mb-4">
                                        <div className="card-header bg-transparent"><h4
                                            className="text-uppercase ls-1 py-3 mb-0"
                                            style={{textColor: '#53607e'}}
                                        >
                                            RIDE pack
                                        </h4>
                                        </div>
                                        <div className="card-body px-lg-7">
                                            <div className="display-2">$23</div>
                                            <span className=" text-muted">per month</span>
                                            <ul className="list-unstyled my-4">
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-terminal"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">8$ per additional GB</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-pen-fancy"/></div>
                                                        </div>
                                                        <div><span
                                                            className="pl-2 text-sm">More than 150 countries</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-hdd"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">3GB data included</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <button type="button"
                                                    className="btn btn-primary mb-3"
                                                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                    onClick={handlePaketa3}
                                            >
                                                Purchase
                                            </button>
                                        </div>
                                        <div className="card-footer"><a href="http://localhost:3000/price#!"
                                                                        className=" text-muted">Unlimited threads</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="card card-pricing border-0 text-center mb-4">
                                        <div className="card-header bg-transparent"><h4
                                            className="text-uppercase ls-1 py-3 mb-0"
                                            style={{textColor: '#53607e'}}

                                        >ALPHA PACK</h4>
                                        </div>
                                        <div className="card-body px-lg-7">
                                            <div className="display-2">$37</div>
                                            <span className=" text-muted">per month</span>
                                            <ul className="list-unstyled my-4">
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-terminal"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">7.5$ per additional GB</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-pen-fancy"/></div>
                                                        </div>
                                                        <div><span
                                                            className="pl-2 text-sm">More than 150 countries</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-hdd"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">5GB data included</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <button type="button"
                                                    className="btn btn-primary mb-3"
                                                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                    onClick={handlePaketa5}
                                            >
                                                Purchase
                                            </button>
                                        </div>
                                        <div className="card-footer"><a href="http://localhost:3000/price#!"
                                                                        className=" text-muted">Unlimited threads</a>
                                        </div>
                                    </div>
                                </div>
                                {/*<div className="col-lg-6">*/}
                                {/*    <div className="card card-pricing border-0 text-center mb-4">*/}
                                {/*        <div className="card-header bg-transparent"><h4*/}
                                {/*            className="text-uppercase ls-1 py-3 mb-0"*/}
                                {/*            style={{textColor: '#53607e'}}*/}

                                {/*        >BRAVO PACK</h4>*/}
                                {/*        </div>*/}
                                {/*        <div className="card-body px-lg-7">*/}
                                {/*            <div className="display-2">$70</div>*/}
                                {/*            <span className=" text-muted">per month</span>*/}
                                {/*            <ul className="list-unstyled my-4">*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-terminal"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span className="pl-2 text-sm">10$ per additional GB</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-pen-fancy"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span*/}
                                {/*                            className="pl-2 text-sm">More than 150 countries</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-hdd"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span className="pl-2 text-sm">7GB data included</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*            </ul>*/}
                                {/*            <button type="button"*/}
                                {/*                    className="btn btn-primary mb-3"*/}
                                {/*                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}*/}
                                {/*                    onClick={handlePaketa7}*/}
                                {/*            >*/}
                                {/*                Purchase*/}
                                {/*            </button>*/}
                                {/*        </div>*/}
                                {/*        <div className="card-footer"><a href="http://localhost:3000/price#!"*/}
                                {/*                                        className=" text-muted">Unlmited threads</a>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                <div className="col-lg-6">
                                    <div className="card card-pricing border-0 text-center mb-4">
                                        <div className="card-header bg-transparent"><h4
                                            className="text-uppercase ls-1 py-3 mb-0"
                                            style={{textColor: '#53607e'}}
                                        >
                                            Bravo pack
                                        </h4>
                                        </div>
                                        <div className="card-body px-lg-7">
                                            <div className="display-2">$70</div>
                                            <span className=" text-muted">per month</span>
                                            <ul className="list-unstyled my-4">
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-terminal"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">7$ per additional GB</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-pen-fancy"/></div>
                                                        </div>
                                                        <div><span
                                                            className="pl-2 text-sm">More than 150 countries</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <div
                                                                className="icon icon-xs icon-shape text-white shadow rounded-circle"
                                                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}

                                                            >
                                                                <i className="fas fa-hdd"/></div>
                                                        </div>
                                                        <div><span className="pl-2 text-sm">10GB data included</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <button type="button"
                                                    className="btn btn-primary mb-3"
                                                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                    onClick={handlePaketa10}
                                            >
                                                Purchase
                                            </button>
                                        </div>
                                        <div className="card-footer"><a href="http://localhost:3000/price#!"
                                                                        className=" text-muted">Unlimited threads</a>
                                        </div>
                                    </div>
                                </div>
                                {/*<div className="col-lg-6">*/}
                                {/*    <div className="card card-pricing border-0 text-center mb-4">*/}
                                {/*        <div className="card-header bg-transparent"><h4*/}
                                {/*            className="text-uppercase ls-1 py-3 mb-0"*/}
                                {/*            style={{textColor: '#53607e'}}*/}

                                {/*        >PURE PACK</h4>*/}
                                {/*        </div>*/}
                                {/*        <div className="card-body px-lg-7">*/}
                                {/*            <div className="display-2">$200</div>*/}
                                {/*            <span className=" text-muted">per month</span>*/}
                                {/*            <ul className="list-unstyled my-4">*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-terminal"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span className="pl-2 text-sm">10$ per additional GB</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-pen-fancy"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span*/}
                                {/*                            className="pl-2 text-sm">More than 150 countries</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-hdd"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span className="pl-2 text-sm">20GB data included</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*            </ul>*/}
                                {/*            <button type="button"*/}
                                {/*                    className="btn btn-primary mb-3"*/}
                                {/*                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}*/}
                                {/*                    onClick={handlePaketa20}*/}
                                {/*            >*/}
                                {/*                Purchase*/}
                                {/*            </button>*/}
                                {/*        </div>*/}
                                {/*        <div className="card-footer"><a href="http://localhost:3000/price#!"*/}
                                {/*                                        className=" text-muted">Unlmited threads</a>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                {/*<div className="col-lg-6">*/}
                                {/*    <div className="card card-pricing border-0 text-center mb-4">*/}
                                {/*        <div className="card-header bg-transparent"><h4*/}
                                {/*            className="text-uppercase ls-1 py-3 mb-0"*/}
                                {/*            style={{textColor: '#53607e'}}*/}

                                {/*        >WAVE PACK</h4>*/}
                                {/*        </div>*/}
                                {/*        <div className="card-body px-lg-7">*/}
                                {/*            <div className="display-2">$490</div>*/}
                                {/*            <span className=" text-muted">per month</span>*/}
                                {/*            <ul className="list-unstyled my-4">*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-terminal"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span className="pl-2 text-sm">10$ per additional GB</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-pen-fancy"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span*/}
                                {/*                            className="pl-2 text-sm">More than 150 countries</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*                <li>*/}
                                {/*                    <div className="d-flex align-items-center">*/}
                                {/*                        <div>*/}
                                {/*                            <div*/}
                                {/*                                className="icon icon-xs icon-shape text-white shadow rounded-circle"*/}
                                {/*                                style={{borderColor: '#53607e',backgroundColor: '#fdbb30'}}*/}

                                {/*                            >*/}
                                {/*                                <i className="fas fa-hdd"/></div>*/}
                                {/*                        </div>*/}
                                {/*                        <div><span className="pl-2 text-sm">50GB data included</span>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                </li>*/}
                                {/*            </ul>*/}
                                {/*            <button type="button"*/}
                                {/*                    className="btn btn-primary mb-3"*/}
                                {/*                    style={{borderColor: '#53607e', backgroundColor: '#53607e'}}*/}
                                {/*                    onClick={handlePaketa50}*/}
                                {/*            >*/}
                                {/*                Purchase*/}
                                {/*            </button>*/}
                                {/*        </div>*/}
                                {/*        <div className="card-footer"><a href="http://localhost:3000/price#!"*/}
                                {/*                                        className=" text-muted">Unlmited threads</a>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                            </div>
                        </div>

                    </div>
                </>
            )
    )
};
