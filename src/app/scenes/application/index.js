import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Lending} from "./Lending";
import {Price} from "./Price";
import {Dashboard} from "./Dashboard";
import {GenerateProxy} from "./GenerateProxy"
import {Invoices} from "./Invoices";

const Application = ({history}) => {

    return (
        <>
            <Router history={history}>
                <Route exact path={'/'} component={Lending}/>
                <Route exact path={'/dashboard/'} component={Dashboard}/>
                <Route exact path={'/price'} component={Price}/>
                <Route exact path={'/generateproxy'} component={GenerateProxy}/>
                <Route exact path={'/invoices'} component={Invoices}/>
            </Router>
        </>
    )
};

export default Application;
