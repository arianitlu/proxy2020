import React, {useState} from "react";
import {NavbarVertical} from "../../components/NavbarVertical";
import {TopNav} from "../../components/TopNav";
import {getUser} from "../../helpers/web-storage-controller";

export const ChangePassword = () => {

    const [values, setValues] = useState({oldPassword: "", newPassword: ""});
    const [success, setSuccess] = useState();

    const changePassword = () => {
        const formData = new FormData();
        formData.append("old_password", values.oldPassword);
        formData.append("new_password", values.newPassword);
        formData.append("confirm_password", values.newPassword);


        fetch(
            `https://rareproxy.io/back/public/api/change_password`,
           
            {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Headers":
                        " Origin, Content-Type, X-Auth-Token,X-Requested-With,Content-Type, Accept",
                    "Access-Control-Allow-Credentials": "true",
                    "Authorization": "Bearer " + getUser()
                },
                body: formData,
            }
        )
            .then((response) => response.json())
            .then((data) => {
                if (!!data.status && data.status === 200) {
                    setSuccess(true)
                } else {
                    setSuccess(false)
                }
            });
    };

    const handleChangePassword = (e) => {
        setValues({
            ...values,
            oldPassword: e.target.value
        });
    };

    const handleChangeNewPassword = (e) => {
        setValues({
            ...values,
            newPassword: e.target.value
        });
    };

    const submit = () => {
        changePassword();
    };


    return (

        getUser() === null ? window.location.assign("/login")
            :
            (

                <>

                    <NavbarVertical/>

                    <div className="main-content" id="panel">
                        {/* Topnav */}
                        <TopNav/>
                        {/* Header */}
                        {/* Header */}
                        <div className="header bg-primary pb-6">
                            <div className="container-fluid">
                                <div className="header-body">
                                    <div className="row align-items-center py-4">
                                        <div className="col-lg-6 col-7">
                                            <h6 className="h2 text-white d-inline-block mb-0">Home</h6>
                                            <nav aria-label="breadcrumb" className="d-none d-md-inline-block ml-md-4">
                                                <ol className="breadcrumb breadcrumb-links breadcrumb-dark">
                                                    <li className="breadcrumb-item"><a href="#"><i
                                                        className="fas fa-home"/></a>
                                                    </li>
                                                    <li className="breadcrumb-item"><a href="#">Change password</a></li>
                                                </ol>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Page content */}
                        <div className="container-fluid mt--6">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="card-wrapper">
                                        {/* Form controls */}
                                        <div className="card">
                                            {/* Card header */}
                                            <div className="card-header">
                                                <h3 className="mb-0">Change password of your account</h3>
                                            </div>
                                            {/* Card body */}
                                            <div className="card-body">
                                                <form>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlInput1">Old password</label>
                                                        <input
                                                            type="password"
                                                            className="form-control"
                                                            id="exampleFormControlInput1"
                                                            placeholder="*******"
                                                            value={values.oldPassword}
                                                            onChange={handleChangePassword}
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlInput2">New password</label>
                                                        <input
                                                            type="password"
                                                            className="form-control"
                                                            id="exampleFormControlInput2"
                                                            placeholder="*******"
                                                            value={values.newPassword}
                                                            onChange={handleChangeNewPassword}
                                                        />
                                                    </div>
                                                    <button
                                                        className="btn btn-primary"
                                                        type="button"
                                                        onClick={submit}
                                                    >
                                                        Change
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                        {/* HTML5 inputs */}
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="card-wrapper">
                                        {/* Sizes */}
                                        <div className="card">
                                            {/* Card header */}
                                            <div className="card-header">
                                                <h3 className="mb-0">Message</h3>
                                            </div>
                                            {/* Card body */}
                                            <div className="col-md-6">
                                                {
                                                    success === true &&
                                                    <button type="button" className="btn btn-success mb-4">Password
                                                        updated successfully</button>
                                                }

                                                {
                                                    success === false &&
                                                    <button type="button" className="btn btn-danger mb-4">Error while
                                                        changing password</button>
                                                }
                                            </div>
                                        </div>
                                        {/* Textareas */}
                                        {/* Selects */}
                                        {/* File browser */}
                                        {/* Checkboxes and radios */}
                                    </div>
                                </div>
                            </div>
                            {/* Footer */}
                            <footer className="footer pt-0">
                                <div className="row align-items-center justify-content-lg-between">
                                    <div className="col-lg-6">
                                        <div className="copyright text-center  text-lg-left  text-muted">
                                            © 2020 <a href="https://www.creative-tim.com"
                                                      className="font-weight-bold ml-1"
                                                      target="_blank">Creative Tim</a>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <ul className="nav nav-footer justify-content-center justify-content-lg-end">
                                            <li className="nav-item">
                                                <a href="https://www.creative-tim.com" className="nav-link"
                                                   target="_blank">Creative
                                                    Tim</a>
                                            </li>
                                            <li className="nav-item">
                                                <a href="https://www.creative-tim.com/presentation" className="nav-link"
                                                   target="_blank">About Us</a>
                                            </li>
                                            <li className="nav-item">
                                                <a href="http://blog.creative-tim.com" className="nav-link"
                                                   target="_blank">Blog</a>
                                            </li>
                                            <li className="nav-item">
                                                <a href="https://www.creative-tim.com/license" className="nav-link"
                                                   target="_blank">License</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </>
            )

    )
};