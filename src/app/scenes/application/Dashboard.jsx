import React, {useEffect, useState} from "react";
import {NavbarVertical} from "../../components/NavbarVertical";
import {requestCoinbaseHeaders, requestHeaders} from "../../helpers/request-headers";
import {getProxyId, getUser, getUserId} from "../../helpers/web-storage-controller";
import moment from "moment";

import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

export const Dashboard = () => {

        const [values, setValues] = useState({addresses: "", oldPassword: "", newPassword: "", confirmPassword: ""});
        const [userProfile, setUserProfile] = useState();
        const [location, setLocation] = useState({IPv4: "", country_name: ""});
        const [expiryDate, setExpiryDate] = useState("Never");
        const [loader, setLoader] = useState(false);
        const [paketa, setPaketa] = useState("None");

        const getUserProfile = (id) => {

            const objId = {
                "user_id": id
            };

            fetch(
                `https://cors-anywhere.herokuapp.com/https://api.proxiware.com/v1/user/info`,
                {
                    method: "POST",
                    headers: requestHeaders(),
                    body: JSON.stringify(objId),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    setUserProfile(data);
                });
        };

        useEffect(() => {
            getUserProfile(getProxyId())
        }, [getProxyId()]);

        useEffect(() => {
            if (!!userProfile) {
                if (parseInt(userProfile.data_expiry) !== -1) {
                    var fulldate = new Date(userProfile.data_expiry * 1000);
                    var unix_to_date = moment(fulldate).format("DD-MM-YYYY");
                    setExpiryDate(unix_to_date);
                } else {
                    setExpiryDate("NONE");
                }
            }
        }, [userProfile]);

        // Handle Password
        const [success, setSuccess] = useState(false);
        const [wrong, setWrong] = useState(false);


        const changePassword = () => {
            const formData = new FormData();
            formData.append("old_password", values.oldPassword);
            formData.append("new_password", values.newPassword);
            formData.append("confirm_password", values.newPassword);

            fetch(
                `https://rareproxy.io/back/public/api/change_password`,
                {
                    method: "POST",
                    headers: {
                        "Access-Control-Allow-Headers":
                            " Origin, Content-Type, X-Auth-Token,X-Requested-With,Content-Type, Accept",
                        "Access-Control-Allow-Credentials": "true",
                        "Authorization": "Bearer " + getUser()
                    },
                    body: formData,
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    if (!!data.status && data.status === 200) {
                        setSuccess(true)
                    } else {
                        setWrong(true)
                    }
                });
        };

        const handleIpUnbind = (ip) => {
            setLoader(true);
            unBindIpAddress(ip);
        };

        const handleChangePassword = (e) => {
            setValues({
                ...values,
                oldPassword: e.target.value
            });
        };

        const handleChangeNewPassword = (e) => {
            setValues({
                ...values,
                newPassword: e.target.value
            });
        };

        const handleChangeConfirmPassword = (e) => {
            setValues({
                ...values,
                confirmPassword: e.target.value
            });
        };

        const submit = () => {
            changePassword();
        };

        // ----------------------------------------------
        // BIND IP
        const bindIpAddress = () => {
            const objId = {
                "user_id": getProxyId(),
                "addr": values.ip,
            };
            fetch(
                `https://cors-anywhere.herokuapp.com/https://api.proxiware.com/v1/user/binds/bind`,
                {
                    method: "POST",
                    headers: requestHeaders(),
                    body: JSON.stringify(objId),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    setLoader(false);
                    if (data && !data.error) {
                        setValues({
                            ...values,
                            addresses: [...values.addresses, values.ip]
                        });
                        window.location.assign("/dashboard");
                    }
                });
        };

        const unBindIpAddress = (ip) => {
            const objId = {
                "user_id": getProxyId(),
                "addr": ip,
            };
            fetch(
                `https://cors-anywhere.herokuapp.com/https://api.proxiware.com/v1/user/binds/unbind`,
                {
                    method: "POST",
                    headers: requestHeaders(),
                    body: JSON.stringify(objId),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    setLoader(false);
                    if (data && !data.error) {
                        window.location.assign("/dashboard");
                    }
                });
        };

        const handleChangeIp = (e) => {
            setValues({
                ...values,
                ip: e.target.value
            });
        };

        const submitOnBindIp = () => {
            setLoader(true);
            bindIpAddress();
        };

        function getLocation() {
            fetch(
                `https://api.ipdata.co/?api-key=254cc44fa537491e194b3183d6c579531416bf3798c4923e7e4d3467`,
                {}
            )
                .then((response) => response.json())
                .then((data) => {
                    setLocation({
                        IPv4: data.ip,
                        country_name: data.country_name
                    })
                });
        }

        useEffect(() => {
            getLocation()
        }, []);


        // -----------------------------------------------------------------------
        //     GET INVOICES

        const [invoices, setInvoices] = useState();
        const [shuma, setShuma] = useState();

        const getInvoices = () => {
            fetch(
                `https://rareproxy.io/back/public/api/proxy/${getProxyId()}`,
                {
                    method: "GET",
                    headers: {
                        "Access-Control-Allow-Headers":
                            " Origin, Content-Type, X-Auth-Token,X-Requested-With,Content-Type, Accept",
                        "Access-Control-Allow-Credentials": "true",
                        "Authorization": "Bearer " + getUser()
                    },
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    setInvoices(data);
                    checkInvoices(data);
                });
        };

        useEffect(() => {
            getInvoices();
        }, []);

        useEffect(() => {
            if (!!invoices) {
                var shumaCalc = 0;
                for (let i = 0; i < invoices.proxy.length; i++) {
                    if (invoices.proxy[i].package_type_space !== null) {
                        shumaCalc = parseInt(shumaCalc, 10) + parseInt(invoices.proxy[i].package_type_space, 10);
                    }
                }
                setShuma(shumaCalc);

                for (let i = 0; i < invoices.proxy.length; i++) {
                    if (invoices.proxy[i].completed_status !== "Uncompleted") {
                        setPaketa(invoices.proxy[i].package_name);
                        return;
                    }
                }
            }
        }, [invoices]);


        //------------------------------------------------------------------
        // CHECK COINBASE

        // -------------------------------
        // Methods
        function checkInvoices(data) {
            for (let i = 0; i < data.proxy.length; i++) {
                if (data.proxy[i].proxy_data !== null) {
                    checkCoinbase(data.proxy[i].proxy_data);
                }
            }
        }

        function checkCoinbase(checkoutId) {
            fetch(
                `https://api.commerce.coinbase.com/charges/${checkoutId}`,
                {
                    method: "GET",
                    headers: requestCoinbaseHeaders(),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    if (data && !!data.data) {
                        if (!!data.data.payments && data.data.payments.length > 0) {
                            if (data.data.payments[0].status = "CONFIRMED") {
                                if (data.data.name <= parseFloat(data.data.payments[0].value.local.amount)) {
                                    deleteUserInvoice(getUserId(), checkoutId, data.data.name);
                                    //setUserInvoice(parseInt(data.data.name))
                                }
                            }
                        }
                    } else {
                        console.log("ERROR PAYMENT")
                    }
                });
        }

        function purchaseMegabajt(megabajt) {
            const objId = {
                "user_id": getProxyId(),
                "data_string": megabajt + "GB"
            };

            fetch(
                `https://cors-anywhere.herokuapp.com/https://api.proxiware.com/v1/user/data/add`,
                {
                    method: "POST",
                    headers: requestHeaders(),
                    body: JSON.stringify(objId),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    setDataExpire();
                });
        }

        function getPackageBasedOnGB(megabajt) {
            switch (megabajt) {
                case 1:
                    return "Starter Pack";
                    break;
                case 3:
                    return "Ride Pack";
                    break;
                case 5:
                    return "Alpha Pack";
                    break;
                case 10:
                    return "Bravo Pack";
                    break;
            }
        }

        function getPriceBasedOnMegabajt(megabajt) {
            switch (megabajt) {
                case 1:
                    return "8$";
                    break;
                case 3:
                    return "23$";
                    break;
                case 5:
                    return "37$";
                    break;
                case 10:
                    return "70$";
                    break;
            }
        }

        function setUserInvoice(megabajt) {

            var MB = 0;

            switch (megabajt) {
                case 8:
                    MB = 1;
                    break;
                case 23:
                    MB = 3;
                    break;
                case 37:
                    MB = 5;
                    break;
                case 70:
                    MB = 10;
                    break;
            }

            const formDataa = new FormData();
            formDataa.append("user_id", getUserId());
            formDataa.append("proxy_id", getProxyId());
            formDataa.append("package_name", getPackageBasedOnGB(MB));
            formDataa.append("package_amount", getPriceBasedOnMegabajt(MB));
            formDataa.append("completed_status", "Completed");
            formDataa.append("package_type_space", MB);

            fetch(
                `https://rareproxy.io/back/public/api/proxies`,
                {
                    method: "POST",
                    headers: {},
                    body: formDataa,
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    purchaseMegabajt(MB);
                });
        }

        function deleteUserInvoice(userId, checkoudId, addMegabajt) {
            fetch(
                `https://rareproxy.io/back/public/api/delete_checkout?user_id=${userId}&proxy_data=${checkoudId}`,
                {
                    method: "DELETE",
                    headers: {},
                }
            )
                .then((response) => response.json())
                .then((data) => {
                    if (data[0] == "sucess") {
                        setUserInvoice(parseInt(addMegabajt))
                    }
                    //setUserInvoice(parseInt(addMegabajt))
                });
        }

        function setDataExpire() {

            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 2) + '-' + today.getDate();
            var unixDate = moment(date, "YYYY-MM-DD").unix();

            const objId =
                {
                    "user_id": getProxyId(),
                    "data_expiry": unixDate,
                    "error": null
                };

            fetch(
                `https://cors-anywhere.herokuapp.com/https://api.proxiware.com/v1/user/data/setExpiry`,
                {
                    method: "POST",
                    headers: requestHeaders(),
                    body: JSON.stringify(objId),
                }
            )
                .then((response) => response.json())
                .then((data) => {
                });
        }

        return (
            getUser() === null ? window.location.assign("/login")
                :
                (
                    <>
                        <NavbarVertical/>

                        <div className="main-content" id="panel">
                            {/* Topnav */}
                            {/*<TopNav/>*/}
                            {/* Header */}
                            {/* Header */}


                            <nav className="navbar navbar-top navbar-expand navbar-dark border-bottom">
                                <div className="container-fluid">
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        {/* Navbar links */}
                                        <ul className="navbar-nav align-items-center  ml-md-auto ">
                                            <li className="nav-item d-xl-none">
                                                {/* Sidenav toggler */}
                                                <div
                                                    className="pr-3 sidenav-toggler sidenav-toggler-dark"
                                                    data-action="sidenav-pin"
                                                    data-target="#sidenav-main"
                                                >
                                                    <div className="sidenav-toggler-inner">
                                                        <i className="sidenav-toggler-line" style={{background: 'black'}}/>
                                                        <i className="sidenav-toggler-line" style={{background: 'black'}}/>
                                                        <i className="sidenav-toggler-line" style={{background: 'black'}}/>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>

                            <div className="header pb-4">
                                <div className="container-fluid">
                                    <div className="header-body">
                                        <div className="row align-items-center py-4">
                                            <div className="col-lg-6 col-7">
                                                <h6 className="h2 d-inline-block mb-0">Dashboard</h6>
                                                <nav aria-label="breadcrumb" className="d-none d-md-inline-block ml-md-4">
                                                    <ol className="breadcrumb breadcrumb-links">
                                                        <li className="breadcrumb-item"><a
                                                            href="https://demos.creative-tim.com/argon-dashboard-pro/pages/dashboards/alternative.html#"/>
                                                        </li>
                                                        <li className="breadcrumb-item"/>
                                                    </ol>
                                                </nav>
                                            </div>
                                            <div className="col-lg-6 col-5 text-right">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Page content */}
                            <div className="container-fluid mt--6">
                                <div className="row">
                                    <div className="col-xl-3 col-md-6">
                                        <div className="card bg-gradient-primary border-0">
                                            {/* Card body */}
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col">
                                                        <h5 className="card-title text-uppercase text-muted mb-0 text-white">Package</h5>
                                                        <span className="h2 font-weight-bold mb-0 text-white">
                                                            {!!paketa ? paketa : "None"}
                                                        </span>
                                                        <div className="progress progress-xs mt-3 mb-0">
                                                            <div className="progress-bar bg-success"
                                                                 role="progressbar" aria-valuenow={30}
                                                                 aria-valuemin={0} aria-valuemax={100}
                                                                 style={{width: !!invoices ? '100%' : '0%'}}/>
                                                        </div>
                                                    </div>
                                                    <div className="col-auto">
                                                        <button type="button"
                                                                className="btn btn-sm btn-neutral mr-0"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            {!!shuma ? shuma : "0"} GB
                                                        </button>
                                                    </div>
                                                </div>
                                                <p className="mt-3 mb-0 text-sm">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-md-6">
                                        <div className="card bg-gradient-info border-0">
                                            {/* Card body */}
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col">
                                                        <h5 className="card-title text-uppercase text-muted mb-0 text-white">DATA
                                                            USED</h5>
                                                        <span
                                                            className="h2 font-weight-bold mb-0 text-white">
                                                            {!!userProfile && !!shuma ? Number(shuma - (userProfile.data / 1000000000)).toFixed(2) : "0"} GB</span>
                                                        <div className="progress progress-xs mt-3 mb-0">
                                                            <div className="progress-bar bg-success"
                                                                 role="progressbar" aria-valuenow={50}
                                                                 aria-valuemin={0} aria-valuemax={100}
                                                                 style={{width: !!invoices && invoices.proxy ? invoices.proxy[0].package_type_space : '0'}}/>
                                                        </div>
                                                    </div>
                                                    <div className="col-auto">
                                                        <button type="button"
                                                                className="btn btn-sm btn-neutral mr-0"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            {!!shuma && !!userProfile ? Number(((shuma - (userProfile.data / 1000000000)) / shuma) * 100).toFixed(2) : "0"}%
                                                        </button>
                                                    </div>
                                                </div>
                                                <p className="mt-3 mb-0 text-sm">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-md-6">
                                        <div className="card bg-gradient-danger border-0">
                                            {/* Card body */}
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col">
                                                        <h5 className="card-title text-uppercase text-muted mb-0 text-white">expired
                                                            date</h5>
                                                        <span
                                                            className="h2 font-weight-bold mb-0 text-white">
                                                            {expiryDate}
                                                        </span>
                                                        <div className="progress progress-xs mt-3 mb-0">
                                                            <div className="progress-bar bg-success"
                                                                 role="progressbar" aria-valuenow={80}
                                                                 aria-valuemin={0} aria-valuemax={100}
                                                                 style={{width: '0%'}}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p className="mt-3 mb-0 text-sm">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-md-6">
                                        <div className="card bg-gradient-default border-0">
                                            {/* Card body */}
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col">
                                                        <h5 className="card-title text-uppercase text-muted mb-0 text-white">
                                                            Your current IP ADRESS
                                                        </h5>
                                                        <span className="h2 font-weight-bold mb-0 text-white">
                                                            {location && location.IPv4}
                                                        </span>
                                                    </div>
                                                    <div className="col-auto">
                                                        <button type="button"
                                                                className="btn btn-sm btn-neutral mr-0"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            {location && location.country_name}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-deck flex-column flex-xl-row">
                                    <div className="card">

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-8">
                                        {/* Members list group card */}

                                        <div className="card">
                                            <div className="card-header border-0">
                                                <div className="row align-items-center">
                                                    <div className="col">
                                                        <h3 className="mb-0">WHITELIST YOUR IP</h3>
                                                    </div>
                                                    <div className="col text-right">
                                                        <input type="text" className="form-control form-control-sm"
                                                               id="addr" name="addr" required
                                                               pattern="^([0-9]{1,3}\.){3}[0-9]{1,3}$"
                                                               onChange={handleChangeIp}
                                                        />
                                                    </div>
                                                    <div className="col text-right">
                                                        <div
                                                            className="btn btn-sm btn-primary btn-round btn-icon"
                                                            data-toggle="tooltip"
                                                            data-original-title="Edit product"
                                                            style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                            onClick={submitOnBindIp}
                                                        >
                                                        <span className="btn-inner--icon">
                                                            <i className="fas fa-user-edit"></i>
                                                        </span>
                                                            <span className="btn-inner--text">
                                                                Bind IP
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                {loader &&
                                                <div className="align-items-center">
                                                    <Loader
                                                        type="TailSpin"
                                                        color="#000"
                                                        height={30}
                                                        width={30}
                                                    />
                                                </div>
                                                }
                                            </div>
                                            <div className="table-responsive">
                                                {/* Projects table */}
                                                <div className="table-responsive">
                                                    <div className="col-sm-12 col-md-6">
                                                        <div id="data-table_filter" className="dataTables_filter">
                                                            <label><label/></label></div>
                                                    </div>
                                                    <table className="table align-items-center table-flush">
                                                        <thead className="thead-light">
                                                        <tr>
                                                            <th scope="col" className="sort" data-sort="name">Authorize your
                                                                IP address so that our servers can authenticate you.
                                                            </th>
                                                            <th className="sorting" tabIndex="0" aria-controls="data-table"
                                                                rowSpan="1" colSpan="1"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody className="list">
                                                        <tr>
                                                            <th scope="row">
                                                                <div className="media align-items-center">
                                                                    <div className="media-body">
                                                                        Address
                                                                    </div>
                                                                </div>
                                                            </th>
                                                            <th className="sorting" tabIndex={0} aria-controls="data-table"
                                                                rowSpan={1} colSpan={1}
                                                                aria-label="Action: activate to sort column ascending"
                                                                style={{width: '271.8px'}}>Action
                                                            </th>
                                                        </tr>
                                                        {
                                                            userProfile && !!userProfile.binds &&
                                                            userProfile.binds.map((ip, index) => {
                                                                return (
                                                                    <tr role="row" className="odd">
                                                                        <td>{ip}</td>
                                                                        <td onClick={() => handleIpUnbind(ip)}>
                                                                            <div
                                                                                className="btn btn-sm btn-danger btn-round btn-icon"
                                                                                data-toggle="tooltip"
                                                                                data-original-title="Edit product"
                                                                            >
                                                                            <span className="btn-inner--icon"><i
                                                                                className="fas fa-trash"></i></span>
                                                                                <span
                                                                                    className="btn-inner--text">Delete</span>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-4">
                                        {/* Checklist */}
                                        <div className="card">
                                            {/* Card header */}
                                            <div className="card-header">
                                                <h3 className="mb-0">Change password of your account</h3>
                                            </div>
                                            {/* Card body */}
                                            <div className="card-body">
                                                <form>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlInput1">Old password</label>
                                                        <input
                                                            type="password"
                                                            className="form-control"
                                                            id="exampleFormControlInput1"
                                                            placeholder="*******"
                                                            value={values.oldPassword}
                                                            onChange={handleChangePassword}
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlInput2">New password</label>
                                                        <input
                                                            type="password"
                                                            className="form-control"
                                                            id="exampleFormControlInput2"
                                                            placeholder="*******"
                                                            value={values.newPassword}
                                                            onChange={handleChangeNewPassword}
                                                        />
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlInput3">Confirm password</label>
                                                        <input
                                                            type="password"
                                                            className="form-control"
                                                            id="exampleFormControlInput3"
                                                            placeholder="*******"
                                                            value={values.confirmPassword}
                                                            onChange={handleChangeConfirmPassword}
                                                        />
                                                    </div>
                                                    <button
                                                        className="btn btn-primary"
                                                        type="button"
                                                        onClick={submit}
                                                        style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                    >
                                                        Change
                                                    </button>
                                                    {!!success && success === true &&
                                                    <button type="button" className="btn btn-primary btn-block mt-3">
                                                        Password Changed Successfully
                                                    </button>
                                                    }
                                                    {!!wrong && wrong === true &&
                                                    <button type="button" className="btn btn-danger btn-block mt-3">
                                                        Error while changing the pasword
                                                    </button>
                                                    }
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Footer */}
                            </div>
                        </div>

                    </>
                )
        )
    }
;

