import React, {useEffect, useState} from "react";
import {NavbarVertical} from "../../components/NavbarVertical";
import {getProxyId, getUser} from "../../helpers/web-storage-controller";
import 'react-coinbase-commerce/dist/coinbase-commerce-button.css';

export const Invoices = () => {

    const [invoices, setInvoices] = useState();

    const getInvoices = () => {
        fetch(
            `https://rareproxy.io/back/public/api/proxy/${getProxyId()}`,
            {
                method: "GET",
                headers: {
                    "Access-Control-Allow-Headers":
                        " Origin, Content-Type, X-Auth-Token,X-Requested-With,Content-Type, Accept",
                    "Access-Control-Allow-Credentials": "true",
                    "Authorization": "Bearer " + getUser()
                },
            }
        )
            .then((response) => response.json())
            .then((data) => {
                setInvoices(data);
            });
    };

    useEffect(() => {
        getInvoices();
    }, []);

    return (
        getUser() === null ? window.location.assign("/login")
            :
            (
                <>

                    <NavbarVertical/>
                    <div className="main-content" id="panel">
                        {/* Topnav */}
                        {/*<TopNav/>*/}
                        <nav className="navbar navbar-top navbar-expand" style={{background: '#54617e'}}>
                            <div className="container-fluid">
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    {/* Navbar links */}
                                    <ul className="navbar-nav align-items-center  ml-md-auto ">
                                        <li className="nav-item d-xl-none">
                                            {/* Sidenav toggler */}
                                            <div
                                                className="pr-3 sidenav-toggler sidenav-toggler-dark"
                                                data-action="sidenav-pin"
                                                data-target="#sidenav-main"
                                            >
                                                <div className="sidenav-toggler-inner">
                                                    <i className="sidenav-toggler-line"/>
                                                    <i className="sidenav-toggler-line"/>
                                                    <i className="sidenav-toggler-line"/>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        {/* Header */}
                        {/* Header */}
                        {/* Page content */}

                        <div className="header pb-4"
                             style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                        >
                            <div className="container-fluid">
                                <div className="header-body">
                                    <div className="row align-items-center py-4">
                                        <div className="col-lg-6 col-7"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="container-fluid mt--6">
                            <div className="row">
                                <div className="col">
                                    <div className="card">
                                        <div className="table-responsive">
                                            <table className="table align-items-center table-flush">
                                                <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort" data-sort="name">Package</th>
                                                    <th scope="col" className="sort" data-sort="budget">Budget</th>
                                                    <th scope="col" className="sort" data-sort="status">Status</th>
                                                    <th scope="col" className="sort" data-sort="completion">Completion
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody className="list">
                                                {!!invoices && invoices.proxy.map((invoice, index) => {
                                                    return (
                                                        invoice.completed_status !== null &&
                                                        <tr key={index}>
                                                            <th scope="row">
                                                                <div className="media align-items-center">
                                                                    <a href="#"
                                                                       className="avatar rounded-circle mr-3">
                                                                        <img alt="Image placeholder"
                                                                             src={'../assets/img/bitcoin.png'}/>
                                                                    </a>
                                                                    <div className="media-body">
                                                                        <span className="name mb-0 text-sm">
                                                                            {!!invoice.package_name ? invoice.package_name : "-"}
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </th>
                                                            <td className="budget">
                                                                {!!invoice.package_amount ? invoice.package_amount :  "-"}
                                                            </td>
                                                            <td>
                      <span className="badge badge-dot mr-4">
                        <i className="bg-warning"/>
                        <span className="status">{invoice.completed_status}</span>
                      </span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-center">
                                                                            <span
                                                                                className="completion mr-2">{invoice.completed_status === "Completed" ? "100%" : "50%"}</span>
                                                                    <div>
                                                                        <div className="progress">
                                                                            <div
                                                                                className={invoice.completed_status === "Completed" ? "progress-bar bg-green" : "progress-bar bg-danger"}
                                                                                role="progressbar"
                                                                                aria-valuenow={50}
                                                                                aria-valuemin={0}
                                                                                aria-valuemax={100}
                                                                                style={{width: invoice.completed_status === "Completed" ? '100%' : '50%'}}/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </>
            )
    )
};
