import React from "react";
import {NavBar} from "../auth/components/Navbar";
import Helmet from "react-helmet";

export const Lending = () => {
    const goToLogin = () => {
        window.location.assign("/login");
    };

    return (
        <>
            <Helmet>
                <title>Rareproxy - fastest residential proxies</title>
                <meta name="description" content="Helmet application"/>
            </Helmet>

            <NavBar/>

            <section
                className="pricing-background--rpn"
                style={{backgroundColor: 'turquoise'}}
            >
                <header className="header header--pricing">
                    <div className="hero-foot header__footer">
                        <div className="container pricing-header pricing-header--rpn columns">
                            <div className="column pricing-header__promo is-5 is-12-touch">
                                <div className="breadcrumbs-container ">
                                    <div className="breadcrumbs-nav">
                                        <a className="breadcrumbs-nav__item
        breadcrumbs-nav__item--dark" href="https://rareproxy.io/">
                                            Rareproxy
                                        </a>
                                        <span className="breadcrumbs-nav__item breadcrumbs-nav__item--disabled
          breadcrumbs-nav__item--dark">
Residential Proxies
</span>
                                    </div>
                                </div>
                                <h1 className="pricing-header__promo__title"
                                    style={{color: 'white'}}
                                >
                                    Residential Proxies
                                </h1>
                                <ul className="hero-list">
                                    <li className="has-checkmark has-checkmark">
                                        We provide pool with over than 50 milion residential IP
                                    </li>
                                    <li className="has-checkmark has-checkmark">
                                        Fastest proxy provider in the market
                                    </li>
                                    <li className="has-checkmark has-checkmark">
                                        24/7 live chat support for quick answers
                                    </li>
                                    <li className="has-checkmark has-checkmark">
                                        Buy immediately via "Purchase"
                                    </li>
                                </ul>
                                <button className="
    button
    js-button-dash
    fast-checkout-button


    is-fixed-width
  " data-button-name="Hero-Fast Checkout"
                                        style={{backgroundColor: 'white'}}
                                        onClick={goToLogin}

                                >
                                    <svg aria-hidden="true" focusable="false" width="20" height="20"
                                         viewBox="0 0 20 20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M5 4h4L7.984 7.047 2 7m4 6h9l4-6h-8" stroke="#130F35"
                                                  stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="5.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="12.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <path stroke="#130F35" stroke-width="2" stroke-linecap="round"
                                                  d="M7 10H4"></path>
                                        </g>
                                    </svg>
                                    Purchase
                                </button>
                            </div>
                            <img src="https://i.imgur.com/HjvB4WD.png" className="column is-5 is-hidden-touch"/>
                        </div>
                    </div>
                </header>
                <section className="
    section
    price-cards-container
    price-cards-container--rpn
  " id="rpPriceCardsContainer">
                    <div className="container">
                        <h2 className="price-cards-container__title">Pricing plans</h2>
                        <section className="price-cards-container__list">
                            <div className="pricing-card pricing-card--rpn" data-highlight-text=""
                                 style={{backgroundColor: '#20b2aa'}}
                            >
                                <div className="pricing-card-header">
                                    <h3 className="pricing-card__name"
                                        style={{color: 'white'}}
                                    >
                                        Starter
                                    </h3>
                                    <p className="pricing-card__size pricing-card__size--rpn">
                                        <b>1 GB </b>
                                        included
                                    </p>
                                </div>
                                <div className="pricing-card-body">
                                    <p className="pricing-card__price
                            ">8</p>
                                    <p className="pricing-card__price-subtitle">per month</p>
                                </div>
                                <p className="pricing-card__includes">+ More than 150 countries</p>
                                <div className="pricing-card__price-wrapper">
                                    <p className="pricing-card__package-price">IP whitelist system</p>
                                    <p className="pricing-card__package-price">24/7 support</p>
                                    <p className="pricing-card__package-price">Never get blocked</p>
                                </div>
                                <button data-pricing-plan="Enterprise" className="
    button
    js-button-dash
    is-primary
        is-outlined          " data-button-name="PricingCard-Enterprise-Sign up"
                                        style={{backgroundColor: 'white', color: '#000'}}
                                        onClick={goToLogin}
                                >
                                    <svg aria-hidden="true" focusable="false" width="20" height="20"
                                         viewBox="0 0 20 20"
                                         style={{marginRight: '5px'}}
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M5 4h4L7.984 7.047 2 7m4 6h9l4-6h-8" stroke="#130F35"
                                                  stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="5.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="12.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <path stroke="#130F35" stroke-width="2" stroke-linecap="round"
                                                  d="M7 10H4"></path>
                                        </g>
                                    </svg>
                                    Purchase
                                </button>
                            </div>
                            <div className="
        pricing-card
        pricing-card--rpn
                    " data-highlight-text=""
                                 style={{backgroundColor: '#20b2aa'}}

                            >
                                <div className="pricing-card-header">
                                    <h3 className="pricing-card__name"
                                        style={{color: 'white'}}
                                    >Ride</h3>
                                    <p className="pricing-card__size pricing-card__size--rpn">
                                        <b>3 GB </b>
                                        included
                                    </p>
                                </div>
                                <div className="pricing-card-body">
                                    <p className="pricing-card__price
                            ">23</p>
                                    <p className="pricing-card__price-subtitle">per month</p>
                                </div>
                                <p className="pricing-card__includes">+ More than 150 countries</p>
                                <div className="pricing-card__price-wrapper">
                                    <p className="pricing-card__package-price">IP whitelist system</p>
                                    <p className="pricing-card__package-price">24/7 support</p>
                                    <p className="pricing-card__package-price">Never get blocked</p>
                                </div>
                                <button data-pricing-plan="Pro" className="
    button
    js-button-dash
    is-primary
        is-outlined          " data-button-name="PricingCard-Pro-Sign up"
                                        style={{backgroundColor: 'white', color: '#000'}}
                                        onClick={goToLogin}

                                >
                                    <svg aria-hidden="true" focusable="false" width="20" height="20"
                                         viewBox="0 0 20 20"
                                         style={{marginRight: '5px'}}
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M5 4h4L7.984 7.047 2 7m4 6h9l4-6h-8" stroke="#130F35"
                                                  stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="5.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="12.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <path stroke="#130F35" stroke-width="2" stroke-linecap="round"
                                                  d="M7 10H4"></path>
                                        </g>
                                    </svg>
                                    Purchase
                                </button>
                            </div>
                            <div className="
        pricing-card
        pricing-card--rpn
                pricing-card--highlighted    " data-highlight-text="Most popular"
                                 style={{backgroundColor: '#20b2aa', borderColor: '#00ffff'}}
                            >
                                <div className="pricing-card-header">
                                    <h3 className="pricing-card__name"
                                        style={{color: 'white'}}
                                    >Alpha</h3>
                                    <p className="pricing-card__size pricing-card__size--rpn">
                                        <b>5 GB </b>
                                        included
                                    </p>
                                </div>
                                <div className="pricing-card-body">
                                    <p className="pricing-card__price
                            ">37</p>
                                    <p className="pricing-card__price-subtitle">per month</p>
                                </div>
                                <p className="pricing-card__includes">+ More than 150 countries</p>
                                <div className="pricing-card__price-wrapper">
                                    <p className="pricing-card__package-price">IP whitelist system</p>
                                    <p className="pricing-card__package-price">24/7 support</p>
                                    <p className="pricing-card__package-price">Never get blocked</p>
                                </div>
                                <button data-pricing-plan="Basic" className="
    button
    js-button-dash
    fast-checkout-button



  " data-button-name="PricingCard-Basic-Fast Checkout"
                                        style={{backgroundColor: 'white', color: '#000'}}
                                        onClick={goToLogin}

                                >
                                    <svg aria-hidden="true" focusable="false" width="20" height="20"
                                         viewBox="0 0 20 20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M5 4h4L7.984 7.047 2 7m4 6h9l4-6h-8" stroke="#130F35"
                                                  stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="5.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="12.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <path stroke="#130F35" stroke-width="2" stroke-linecap="round"
                                                  d="M7 10H4"></path>
                                        </g>
                                    </svg>
                                    Purchase
                                </button>
                            </div>
                            <div className="
        pricing-card
        pricing-card--rpn
                    " data-highlight-text="" style={{backgroundColor: '#20b2aa'}}
                            >
                                <div className="pricing-card-header">
                                    <h3 className="pricing-card__name"
                                        style={{color: 'white'}}
                                    >Bravo</h3>
                                    <p className="pricing-card__size pricing-card__size--rpn">
                                        <b>10 GB </b>
                                        included
                                    </p>
                                </div>
                                <div className="pricing-card-body">
                                    <p className="pricing-card__price
                            ">70</p>
                                    <p className="pricing-card__price-subtitle">per month</p>
                                </div>
                                <p className="pricing-card__includes">+ More than 150 countries</p>
                                <div className="pricing-card__price-wrapper">
                                    <p className="pricing-card__package-price">IP whitelist system</p>
                                    <p className="pricing-card__package-price">24/7 support</p>
                                    <p className="pricing-card__package-price">Never get blocked</p>
                                </div>
                                <button data-pricing-plan="Entry" className="
    button
    js-button-dash
    fast-checkout-button



  " data-button-name="PricingCard-Entry-Fast Checkout"
                                        style={{backgroundColor: 'white', color: '#000'}}
                                        onClick={goToLogin}

                                >
                                    <svg aria-hidden="true" focusable="false" width="20" height="20"
                                         viewBox="0 0 20 20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M5 4h4L7.984 7.047 2 7m4 6h9l4-6h-8" stroke="#130F35"
                                                  stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"></path>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="5.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <circle fill="#130F35" fill-rule="nonzero" cx="12.5" cy="16.5"
                                                    r="1.5"></circle>
                                            <path stroke="#130F35" stroke-width="2" stroke-linecap="round"
                                                  d="M7 10H4"></path>
                                        </g>
                                    </svg>
                                    Purchase
                                </button>
                            </div>
                        </section>
                        <section className="price-cards-container__payments">
                            <p>We accept these payment methods:</p>
                            <div className="price-cards-container__payments__icons">
                                <a href="#" className="avatar rounded-circle mr-3">
                                    <img alt="Image placeholder" src={'../assets/img/bitcoin.png'}/>
                                </a>
                                <a href="#" className="avatar rounded-circle mr-3">
                                    <img alt="Image placeholder" src={'../assets/img/etherum.png'}/>
                                </a>
                                <a href="#" className="avatar rounded-circle mr-3">
                                    <img alt="Image placeholder" src={'../assets/img/tether.png'}/>
                                </a>
                            </div>
                        </section>
                    </div>
                </section>
                <section className="section">
                    <div className="container">
                        <div className="extras-container"
                        >
                            <h2 className="extras-container__title" style={{color: 'white'}}>With <strong>no
                                additional
                                fees</strong> &amp; included in the
                                price:</h2>
                            <div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Never get blocked</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Advice on scraping</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>24/7 live support</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Quality</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Unlimited threads</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Usage stats dashboard</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Fastest proxy in the market</p>
                                </div>
                                <div
                                    className="extras-container__item extras-container__item--  extras-container__item--4">
                                    <p>Rotating & Sticky</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section text-block text-block--">
                    <div className="container columns">
                        <div className="text-block__card column is-10 is-centered">
                            <h2 className="text-block__card__title"
                                style={{color: 'white'}}
                            >
                                Need more traffic?
                            </h2>
                            <p className="text-block__card__text">If you need more sub-users, whitelisted IPs or
                                simply
                                more traffic,
                                get in touch with us!</p>
                            <button className="button button-open-modal is-primary is-fixed-width"
                                    data-button-name="TextBlock-Book a call">
                                Contact us
                            </button>
                        </div>
                    </div>
                </section>
            </section>
        </>
    )
};
