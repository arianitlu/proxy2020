import React, {useState} from "react";
import {NavbarVertical} from "../../components/NavbarVertical";
import {gatewayList} from "../../helpers/gatewayList"
import {getUser} from "../../helpers/web-storage-controller";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {countryPort} from "../../helpers/countryPort";

export const GenerateProxy = () => {

    const [copied, toggle] = useState(false);

    const toggleCopy = () => {
        toggle(!copied);
    };

    const countryArr = [];
    gatewayList().map((country) => {
        countryArr.push(country.name);
    });

    const [values, setValues] = useState({
        country: countryArr,
        selected: "Albania",
        amount: 1,
        type: "rotating",
    });

    const [proxy, setProxy] = useState([]);

    const generate = () => {
        const objCountry = countryPort(values.selected);

        var generatedValue = "";
        var arrayOfValues = [];

        if (values.type.toLocaleLowerCase() === "rotating") {
            generatedValue = objCountry.proxy + ":" + objCountry.rotating.port_start;
            setProxy(generatedValue);
        }
        else {
            if (objCountry.sticky?.port_start === undefined) {
                generatedValue = "this country doesnt have sticky proxy";
                setProxy(generatedValue);
            } else {
                generatedValue = objCountry.sticky.port_end - objCountry.sticky.port_start;
                for (let i = objCountry.sticky.port_end; i >= objCountry.sticky.port_start; i--) {
                    arrayOfValues.push(objCountry.proxy + ":" + i);
                }
                let text="";
                for (let i=0; i<arrayOfValues.length; i++){
                    text += arrayOfValues[i] + "\n";
                }
                setProxy(text);
            }
        }
    };

    const handleChangeCountry = (e) => {
        setValues({
            ...values,
            selected: e.target.value
        });
    };

    const handleChangeAmount = (e) => {
        setValues({
            ...values,
            amount: e.target.value
        })
    };

    const handleChangeType = (e) => {
        setValues({
            ...values,
            type: e.target.value
        })
    };

    return (

        getUser() === null ? window.location.assign("/login")
            :
            (
                <>
                    <NavbarVertical/>

                    <div className="main-content" id="panel">
                        {/* Topnav */}
                        {/*<TopNav/>*/}
                        <nav className="navbar navbar-top navbar-expand" style={{background: '#54617e'}}>
                            <div className="container-fluid">
                                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                    {/* Navbar links */}
                                    <ul className="navbar-nav align-items-center  ml-md-auto ">
                                        <li className="nav-item d-xl-none">
                                            {/* Sidenav toggler */}
                                            <div
                                                className="pr-3 sidenav-toggler sidenav-toggler-dark"
                                                data-action="sidenav-pin"
                                                data-target="#sidenav-main"
                                            >
                                                <div className="sidenav-toggler-inner">
                                                    <i className="sidenav-toggler-line"/>
                                                    <i className="sidenav-toggler-line"/>
                                                    <i className="sidenav-toggler-line"/>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        {/* Header */}
                        {/* Header */}
                        <div
                            className="header pb-4"
                            style={{backgroundColor: '#53607e'}}
                        >
                            <div className="container-fluid">
                                <div className="header-body">
                                    <div className="row align-items-center py-4">
                                        <div className="col-lg-6 col-7">


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Page content */}
                        <div className="container-fluid mt--6">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="card-wrapper">
                                        {/* Form controls */}

                                        <div className="card">
                                            {/* Card header */}
                                            <div className="card-header">
                                                <h3 className="mb-0">Generate Proxy</h3>
                                            </div>
                                            {/* Card body */}
                                            <div className="card-body">
                                                <form>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlSelect1">Select
                                                            country</label>
                                                        <select
                                                            className="form-control"
                                                            id="exampleFormControlSelect1"
                                                            onChange={handleChangeCountry}
                                                        >
                                                            {
                                                                values.country.map((country, index) => {
                                                                    return (
                                                                        <option key={index}>{country}</option>
                                                                    )
                                                                })
                                                            }
                                                        </select>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlSelect1">Proxy Type</label>
                                                        <select
                                                            className="form-control"
                                                            id="exampleFormControlSelect1"
                                                            onChange={handleChangeType}
                                                        >
                                                            <option>Rotating</option>
                                                            <option>Sticky</option>
                                                        </select>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-control-label"
                                                               htmlFor="exampleFormControlSelect2">Amount</label>
                                                        <input
                                                            type="number"
                                                            className="form-control"
                                                            id="exampleFormControlInput1"
                                                            onChange={handleChangeAmount}
                                                            placeholder="0"/>
                                                    </div>
                                                    <button
                                                        className="btn btn-primary"
                                                        type="button"
                                                        onClick={generate}
                                                        style={{borderColor: '#53607e', backgroundColor: '#53607e'}}
                                                    >
                                                        Generate
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                        {/* HTML5 inputs */}
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="card-wrapper">
                                        <div className="card">
                                            <div className="card-header">
                                                <h3 className="mb-0">Generated Proxies</h3>
                                            </div>
                                            <div className="card-body">
                                                <div className="form-group">

                                                    <textarea
                                                        className="form-control"
                                                        rows="3"
                                                        value={!!proxy ? proxy : ""}
                                                    >
                                                    </textarea>

                                                    {/*<textarea className="form-control">*/}
                                                    {/*{!!proxy ? proxy : "asdasda"}*/}
                                                    {/*</textarea>*/}
                                                </div>
                                                <div className="form-group">
                                                    <CopyToClipboard text={proxy}>
                                                        <span className="btn btn-primary"
                                                              style={{
                                                                  borderColor: '#53607e',
                                                                  backgroundColor: '#53607e'
                                                              }}
                                                        >
                                                            Copy
                                                        </span>
                                                    </CopyToClipboard>
                                                </div>
                                                <div className="form-group">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Footer */}
                        </div>
                    </div>
                </>
            )

    )
};