import React from "react";

export const NavBar2 = () => {
  return (
      <nav id="navbar-main"
           className="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
          <div className="container">
              <a className="navbar-brand" href="/">
                  <img style={{height:"50px"}} src={"https://i.ibb.co/DKxD92T/rareproxy-logo.png"}/>
              </a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse"
                      aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
              </button>
              <div className="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
                  <ul className="navbar-nav align-items-lg-center ml-lg-auto">
                      <li className="nav-item">
                          <a href="/login" className="nav-link">
                              <span className="nav-link-inner--text">Login</span>
                          </a>
                      </li>
                      <li className="nav-item">
                          <a href="/register" className="nav-link">
                              <span className="nav-link-inner--text">Register</span>
                          </a>
                      </li>
                  </ul>
              </div>
          </div>
      </nav>

  )
};