import React, {useEffect, useState} from "react";
import {requestHeaders} from "../../../helpers/request-headers";
import {getProxyId, getUser} from "../../../helpers/web-storage-controller";
import Loader from "react-loader-spinner";


export const RegisterMain = () => {

    const [values, setValues] = useState({name: "", email: "", password: ""});
    const [accessToken, setAccessToken] = useState({});
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        localStorage.clear();
    }, []);

    const create = () => {
        const formData = new FormData();
        formData.append("name", values.name);
        formData.append("email", values.email);
        formData.append("password", values.password);
        formData.append("password_confirmation", values.password);

        fetch(
            `https://rareproxy.io/back/public/api/register`,
            {
                method: "POST",
                headers: {},
                body: formData,
            }
        )
            .then((response) => response.json())
            .then((data) => {
                setAccessToken(data);
            });
    };

    const handleChangeEmail = (e) => {
        setValues({
            ...values,
            email: e.target.value
        });
    };

    const handleChangeName = (e) => {
        setValues({
            ...values,
            name: e.target.value
        });
    };

    const handleChangePassword = (e) => {
        setValues({
            ...values,
            password: e.target.value
        });
    };

    const handleSubmit = () => {
        setLoader(true);
        create()
    };

    useEffect(() => {

        if (!!accessToken.user) {
            const user_id = accessToken.user.id;
            if (!!user_id) {
                fetch(
                    'https://cors-anywhere.herokuapp.com/https://api.proxiware.com/v1/user/create', {
                        method: 'GET',
                        headers: requestHeaders()
                    }
                ).then((response) => response.json())
                    .then((data) => {
                        updateUser(user_id, data.user_id);
                    });

            }
        }
    }, [accessToken]);

    const updateUser = (user_id, proxy_id) => {
        const formDataa = new FormData();
        formDataa.append("user_id", user_id);
        formDataa.append("proxy_id", proxy_id);
        fetch(
            `https://rareproxy.io/back/public/api/proxies`,
            {
                method: "POST",
                headers: {},
                body: formDataa,
            }
        )
            .then((response) => response.json())
            .then((data) => {
                window.location.assign("/login/true");
            });
    };

    return (
        <div className="main-content">

            <div className="header py-7 py-lg-8 pt-lg-9"
                 style={{background: 'linear-gradient(87deg, #6CCECB 0, #218C8D 100%)'}}

            >
                <div className="container">
                    <div className="header-body text-center mb-7">
                        <div className="row justify-content-center">
                            <div className="col-xl-5 col-lg-6 col-md-8 px-5">
                                <h1 className="text-white">Create an account</h1>
                                <p className="text-lead text-white"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="separator separator-bottom separator-skew zindex-100">
                    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                         xmlns="http://www.w3.org/2000/svg">
                        <polygon className="fill-default" points="2560 0 2560 100 0 100"></polygon>
                    </svg>
                </div>
            </div>

            <div className="container mt--8 pb-5">
                <div className="row justify-content-center">
                    <div className="col-lg-6 col-md-8">
                        <div className="card bg-secondary border-0">
                            <div className="card-header bg-transparent pb-5">
                                <div className="text-muted text-center mt-2"><small>Sign up with</small></div>
                            </div>

                            <div className="card-body px-lg-5 py-lg-5">
                                <form role="form">
                                    <div className="form-group">
                                        <div className="input-group input-group-merge input-group-alternative mb-3">
                                            <div className="input-group-prepend">
                                                    <span className="input-group-text"><i
                                                        className="ni ni-hat-3"></i></span>
                                            </div>
                                            <input
                                                className="form-control"
                                                placeholder="Name"
                                                type="text"
                                                value={values.name || ""}
                                                onChange={handleChangeName}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="input-group input-group-merge input-group-alternative mb-3">
                                            <div className="input-group-prepend">
                                                    <span className="input-group-text"><i
                                                        className="ni ni-email-83"></i></span>
                                            </div>
                                            <input
                                                className="form-control"
                                                placeholder="Email"
                                                type="email"
                                                value={values.email || ""}
                                                onChange={handleChangeEmail}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="input-group input-group-merge input-group-alternative">
                                            <div className="input-group-prepend">
                                            <span className="input-group-text"><i
                                                className="ni ni-lock-circle-open"></i></span>
                                            </div>
                                            <input
                                                className="form-control"
                                                placeholder="Password"
                                                type="password"
                                                value={values.password || ""}
                                                onChange={handleChangePassword}
                                            />
                                        </div>
                                    </div>
                                    <div className="row my-4">
                                        <div className="col-12">
                                            <div
                                                className="custom-control custom-control-alternative custom-checkbox">
                                                <input className="custom-control-input" id="customCheckRegister"
                                                       type="checkbox"/>
                                                <label className="custom-control-label"
                                                       htmlFor="customCheckRegister">
                                                    <span className="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button
                                            type="button"
                                            className="btn btn-primary mt-4 mb-3"
                                            onClick={handleSubmit}
                                            style={{background: '#525f7f'}}
                                        >
                                            Create account
                                        </button>
                                        {loader &&
                                        <Loader
                                            type="TailSpin"
                                            color="#000"
                                            height={30}
                                            width={30}
                                        />
                                        }
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};