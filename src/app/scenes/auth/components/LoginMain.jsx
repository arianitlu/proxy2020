import React, {useEffect, useState} from "react";
import {setProxyId, setUser, setUserId} from "../../../helpers/web-storage-controller";
import Loader from "react-loader-spinner";

export const LoginMain = () => {

    const [good, setGood] = useState(false);
    const [loader, setLoader] = useState(false);


    console.log(window.location.href);

    useEffect(() => {
        if (window.location.href == "https://rareproxy.io/login/true" || window.location.href == "http://rareproxy.io/login/true") {
            setGood(true);
        }
    }, [window.location.href]);

    const [accessToken, setAccessToken] = useState();

    const [values, setValues] = useState({email: "", password: ""});
    const [wrong, setWrong] = useState(false);

    useEffect(() => {
        localStorage.clear();
    }, []);

    const getCode = () => {
        const formData = new FormData();
        formData.append("email", values.email);
        formData.append("password", values.password);

        fetch(
            `https://rareproxy.io/back/public/api/login`,
            {
                method: "POST",
                headers: {
                    "Access-Control-Allow-Headers":
                        " Origin, Content-Type, X-Auth-Token,X-Requested-With,Content-Type, Accept",
                    "Access-Control-Allow-Credentials": "true",
                },
                body: formData,
            }
        )
            .then((response) => response.json())
            .then((data) => {
                setAccessToken(data);
                if (!!data.token) {
                    setProxyId(!!data.proxy_id ? data.proxy_id.proxy_id : "");
                    setUser(!!data.token ? data.token : "");
                    setUserId(!!data.user_id ? data.user_id : "")
                } else if (data.message) {
                    setWrong(true);
                }
            });
    };

    const handleSubmit = () => {
        setLoader(true);
        getCode();
    };

    const handleChangeEmail = (e) => {
        setValues({
            ...values,
            email: e.target.value
        });
    };

    const handleChangePassword = (e) => {
        setValues({
            ...values,
            password: e.target.value
        });
    };

    useEffect(() => {
        if (!!accessToken && !!accessToken.token) {
            window.location.assign("/dashboard");
        } else {
            console.log("Error in credentials")
        }
    }, [accessToken]);


    return (
        <div className="main-content">
            <div className="header py-7 py-lg-8 pt-lg-9"
                 style={{background: 'linear-gradient(87deg, #6CCECB 0, #218C8D 100%)'}}
            >
                <div className="container">
                    <div className="header-body text-center mb-7">
                        <div className="row justify-content-center">
                            <div className="col-xl-5 col-lg-6 col-md-8 px-5">
                                <h1 className="text-white">Welcome!</h1>
                                <p className="text-lead text-white"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="separator separator-bottom separator-skew zindex-100">
                    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                         xmlns="http://www.w3.org/2000/svg">
                        <polygon className="fill-default" points="2560 0 2560 100 0 100"></polygon>
                    </svg>
                </div>
            </div>

            <div className="container mt--8 pb-5">
                <div className="row justify-content-center">
                    <div className="col-lg-5 col-md-7">
                        <div className="card bg-secondary border-0 mb-0">
                            <div className="card-body px-lg-5 py-lg-5">

                                <div className="text-center text-muted mb-4">
                                    <small>Sign in with credentials</small>
                                </div>
                                <form role="form">
                                    <div className="form-group mb-3">
                                        <div className="input-group input-group-merge input-group-alternative">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text"><i
                                                    className="ni ni-email-83"></i></span>
                                            </div>
                                            <input
                                                className="form-control"
                                                placeholder="Email"
                                                type="email"
                                                value={values.email || ""}
                                                onChange={handleChangeEmail}
                                                autoComplete="off"
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="input-group input-group-merge input-group-alternative">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text"><i
                                                    className="ni ni-lock-circle-open"></i></span>
                                            </div>
                                            <input
                                                className="form-control"
                                                placeholder="Password"
                                                type="password"
                                                value={values.password || ""}
                                                onChange={handleChangePassword}
                                                autoComplete="off"
                                            />
                                        </div>
                                    </div>
                                    <div className="custom-control custom-control-alternative custom-checkbox">
                                        <input className="custom-control-input" id=" customCheckLogin"
                                               type="checkbox"/>
                                        <label className="custom-control-label" htmlFor=" customCheckLogin">
                                            <span className="text-muted">Remember me</span>
                                        </label>
                                    </div>
                                    <div className="text-center">
                                        <button
                                            type="button"
                                            className="btn btn-primary my-4"
                                            onClick={handleSubmit}
                                            style={{background: '#525f7f'}}
                                        >
                                            Sign in
                                        </button>
                                        {loader &&
                                                <Loader
                                                    type="TailSpin"
                                                    color="#000"
                                                    height={30}
                                                    width={30}
                                                />
                                        }
                                    </div>

                                    {!!wrong && wrong === true &&
                                    <button type="button" className="btn btn-danger btn-block mt-3">
                                        Email or password is wrong
                                    </button>
                                    }

                                    {!!good &&
                                    <button type="button" className="btn btn-success btn-block mt-3">
                                        New account has been created successfully
                                    </button>
                                    }

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};