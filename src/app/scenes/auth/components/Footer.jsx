import React from "react";

export const Footer = () => {
  return (
      <footer className="py-5" id="footer-main">
          <div className="container">
              <div className="row align-items-center justify-content-xl-between">
                  <div className="col-xl-6">
                      <div className="copyright text-center text-xl-left text-muted">
                          &copy; 2020 <a href="https://www.creative-tim.com" className="font-weight-bold ml-1"
                                         target="_blank">Rare Proxy</a>
                      </div>
                  </div>
              </div>
          </div>
      </footer>
  )
};