import React, {useEffect, useState} from "react"
import {LoginMain} from "./components/LoginMain";
import {NavBar2} from "./components/Navbar2";

export const Login = () => {

    return (
        <div className="bg-default g-sidenav-show g-sidenav-pinned">

            <NavBar2/>

            <LoginMain/>

        </div>
    )
};
