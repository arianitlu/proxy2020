import React, {Fragment} from "react";
import {Route} from "react-router-dom";
import {Login} from "./Login";
import {Register} from "./Register";

export default function Auth() {
    return (
        <Fragment>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Fragment>

    );
}