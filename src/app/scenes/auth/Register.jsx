import React from "react"
import {RegisterMain} from "./components/RegisterMain";
import {NavBar2} from "./components/Navbar2";

export const Register = () => {
    return (
        <div className="bg-default g-sidenav-show g-sidenav-pinned">

            <NavBar2/>

            <RegisterMain/>

        </div>
    )
};

