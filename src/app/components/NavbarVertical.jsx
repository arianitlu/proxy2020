import React from "react";

export const NavbarVertical = () => {

    const logout = () => {
        localStorage.clear();
        window.location.assign("/login");
    };

    return (
        <nav className="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white"
             id="sidenav-main">
            <div className="scroll-wrapper scrollbar-inner" style={{position: 'relative'}}>
                <div className="scrollbar-inner scroll-content"
                     style={{height: '1036px', marginBottom: '0px', marginRight: '0px', maxHeight: 'none'}}>
                    {/* Brand */}
                    <div className="sidenav-header  d-flex  align-items-center">
                        <a className="navbar-brand" href="/dashboard">
                            <img style={{maxHeight:'none'}} src={'https://i.ibb.co/Ctz7nJB/bluerare.png'}/>
                        </a>
                        <div className=" ml-auto ">
                            {/* Sidenav toggler */}
                            <div className="sidenav-toggler d-none d-xl-block active" data-action="sidenav-unpin"
                                 data-target="#sidenav-main">
                                <div className="sidenav-toggler-inner">
                                    <i className="sidenav-toggler-line"/>
                                    <i className="sidenav-toggler-line"/>
                                    <i className="sidenav-toggler-line"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="navbar-inner">
                        {/* Collapse */}
                        <div className="collapse navbar-collapse" id="sidenav-collapse-main">
                            {/* Nav items */}
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="/dashboard">
                                        <i className="ni ni-shop text-primary"/>
                                        <span className="nav-link-text">Dashboard</span>
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/generateproxy">
                                        <i className="ni ni-ungroup text-orange"/>
                                        <span className="nav-link-text">Generate Proxy</span>
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/price">
                                        <i className="ni ni-archive-2 text-green"/>
                                        <span className="nav-link-text">Purchase</span>
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="/invoices">
                                        <i className="ni ni-single-copy-04 text-pink"></i>
                                        <span className="nav-link-text">Invoices</span>
                                    </a>
                                </li>
                                <li className="nav-item" onClick={logout}>
                                    <a className="nav-link" href="/price">
                                        <i className="ni ni-ui-04 text-info"/>
                                        <span className="nav-link-text">Log out</span>
                                    </a>
                                </li>
                            </ul>
                            {/* Divider */}
                            {/*<hr className="my-3"/>*/}
                            {/*/!* Heading *!/*/}
                            {/*<h6 className="navbar-heading p-0 text-muted">*/}
                            {/*    <span className="docs-normal">Follow Us</span>*/}
                            {/*    <span className="docs-mini">F</span>*/}
                            {/*</h6>*/}
                            {/*/!* Navigation *!/*/}
                            {/*<ul className="navbar-nav">*/}
                            {/*    <li className="nav-item">*/}
                            {/*        <a className="nav-link"*/}
                            {/*           href="#">*/}
                            {/*            <i className="ni ni-spaceship"/>*/}
                            {/*            <span className="nav-link-text">Facebook</span>*/}
                            {/*        </a>*/}
                            {/*    </li>*/}
                            {/*    <li className="nav-item">*/}
                            {/*        <a className="nav-link"*/}
                            {/*           href="https://www.instagram.com/rareproxy"*/}
                            {/*           target="_blank">*/}
                            {/*            <i className="ni ni-ui-04"/>*/}
                            {/*            <span className="nav-link-text">Instagram</span>*/}
                            {/*        </a>*/}
                            {/*    </li>*/}
                            {/*</ul>*/}

                            <hr className="my-3"/>
                            <ul className="navbar-nav mb-md-3">
                                <li className="nav-item">
                                    <a className="nav-link" href="#">
                                        <img style={{width:'20px',height:'20px',marginRight:'10px'}} src="https://i.ibb.co/GWQDwTX/telegram.png" alt="telegram"/>
                                        <span className="nav-link-text">Telegram</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="scroll-element scroll-x">
                    <div className="scroll-element_outer">
                        <div className="scroll-element_size"/>
                        <div className="scroll-element_track"/>
                        <div className="scroll-bar" style={{width: '0px', left: '0px'}}/>
                    </div>
                </div>
                <div className="scroll-element scroll-y">
                    <div className="scroll-element_outer">
                        <div className="scroll-element_size"/>
                        <div className="scroll-element_track"/>
                        <div className="scroll-bar" style={{height: '0px'}}/>
                    </div>
                </div>
            </div>
        </nav>
    )
};
