import React from "react";

export const TopNav = ({name}) => {

    const logout = () => {
        localStorage.clear();
        window.location.assign("/login");
    };

    const myProfile = () => {
        window.location.assign("/change-password");
    };

    return (
        <nav className="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
            <div className="container-fluid">
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    {/* Search form */}
                    <form className="navbar-search navbar-search-light form-inline mr-sm-3"
                          id="navbar-search-main">
                        <div className="form-group mb-0">
                            <div className="input-group input-group-alternative input-group-merge">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"><i className="fas fa-search"/></span>
                                </div>
                                <input className="form-control" placeholder="Search" type="text"/>
                            </div>
                        </div>
                        <button type="button" className="close" data-action="search-close"
                                data-target="#navbar-search-main" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </form>
                    {/* Navbar links */}
                    <ul className="navbar-nav align-items-center  ml-md-auto ">
                        <li className="nav-item d-xl-none">
                            {/* Sidenav toggler */}
                            <div className="pr-3 sidenav-toggler sidenav-toggler-dark active"
                                 data-action="sidenav-pin" data-target="#sidenav-main">
                                <div className="sidenav-toggler-inner">
                                    <i className="sidenav-toggler-line"/>
                                    <i className="sidenav-toggler-line"/>
                                    <i className="sidenav-toggler-line"/>
                                </div>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <div
                                className="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                                {/* List group */}
                                <div className="list-group list-group-flush">
                                    <a href="#!" className="list-group-item list-group-item-action">
                                        <div className="row align-items-center">
                                            <div className="col-auto">
                                                {/* Avatar */}
                                                <img alt="Image placeholder"
                                                     src="../../assets/img/theme/team-1.jpg"
                                                     className="avatar rounded-circle"/>
                                            </div>
                                            <div className="col ml--2">
                                                <div
                                                    className="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <h4 className="mb-0 text-sm">{!!name ? name : "User"}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#!" className="list-group-item list-group-item-action">
                                        <div className="row align-items-center">
                                            <div className="col-auto">
                                                {/* Avatar */}
                                                <img alt="Image placeholder"
                                                     src="../../assets/img/theme/team-2.jpg"
                                                     className="avatar rounded-circle"/>
                                            </div>
                                            <div className="col ml--2">
                                                <div
                                                    className="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <h4 className="mb-0 text-sm">{!!name ? name : "User"}</h4>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#!" className="list-group-item list-group-item-action">
                                        <div className="row align-items-center">
                                            <div className="col-auto">
                                                {/* Avatar */}
                                                <img alt="Image placeholder"
                                                     src="../../assets/img/theme/team-3.jpg"
                                                     className="avatar rounded-circle"/>
                                            </div>
                                            <div className="col ml--2">
                                                <div
                                                    className="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <h4 className="mb-0 text-sm">{!!name ? name : "User"}</h4>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </a>
                                    <a href="#!" className="list-group-item list-group-item-action">
                                        <div className="row align-items-center">
                                            <div className="col-auto">
                                                {/* Avatar */}
                                                <img alt="Image placeholder"
                                                     src="../../assets/img/theme/team-4.jpg"
                                                     className="avatar rounded-circle"/>
                                            </div>
                                            <div className="col ml--2">
                                                <div
                                                    className="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <h4 className="mb-0 text-sm">{!!name ? name : "User"}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#!" className="list-group-item list-group-item-action">
                                        <div className="row align-items-center">
                                            <div className="col-auto">
                                                {/* Avatar */}
                                                <img alt="Image placeholder"
                                                     src="../../assets/img/theme/team-5.jpg"
                                                     className="avatar rounded-circle"/>
                                            </div>
                                            <div className="col ml--2">
                                                <div
                                                    className="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <h4 className="mb-0 text-sm">{!!name ? name : "User"}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                {/* View all */}
                                <a href="#!"
                                   className="dropdown-item text-center text-primary font-weight-bold py-3">View
                                    all</a>
                            </div>
                        </li>
                    </ul>
                    <ul className="navbar-nav align-items-center  ml-auto ml-md-0 ">
                        <li className="nav-item dropdown">
                            <a className="nav-link pr-0" href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <div className="media align-items-center">
            <span className="avatar avatar-sm rounded-circle">
            <img alt="Image placeholder" src="../../assets/img/theme/team-4.jpg"/>
            </span>
                                    <div className="media-body  ml-2  d-none d-lg-block">
                                        <span className="mb-0 text-sm  font-weight-bold">{!!name ? name : "User"}</span>
                                    </div>
                                </div>
                            </a>
                            <div className="dropdown-menu  dropdown-menu-right ">
                                <div className="dropdown-header noti-title">
                                    <h6 className="text-overflow m-0">Welcome!</h6>
                                </div>
                                <a
                                    href="#!"
                                    className="dropdown-item"
                                    onClick={myProfile}
                                >
                                    <i className="ni ni-single-02"/>
                                    <span>My profile</span>
                                </a>
                                <a href="#!"
                                   className="dropdown-item"
                                   onClick={logout}
                                >
                                    <i className="ni ni-user-run"/>
                                    <span>Logout</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
};