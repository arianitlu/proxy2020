import {gatewayList} from "./gatewayList";

export const countryPort = (takenName) => {
    var gateWayList = gatewayList();

    for (let i = 0; i < gateWayList.length; i++) {
        if (gateWayList[i].name === takenName) {
            return gateWayList[i];
        }
    }
};
