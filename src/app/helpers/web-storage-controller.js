export const setUser = (token) => {
    try {
        localStorage.setItem("token", token);
    } catch (error) {
        return null;
    }
};

export function getUser(){
    const token = localStorage.getItem("token");
    return token;
}

export const setProxyId = (prxId) => {
    try {
        localStorage.setItem("proxy_id", prxId);
    } catch (error) {
        return null;
    }
};

export function getProxyId(){
    const proxyId = localStorage.getItem("proxy_id");
    return parseInt(proxyId);
}

export const setUserId = (id) => {
    try {
        localStorage.setItem("user_id", id);
    } catch (error) {
        return null;
    }
};

export function getUserId(){
    const proxyId = localStorage.getItem("user_id");
    return parseInt(proxyId);
}

// export const setPaketa = (paketa) => {
//     try {
//         localStorage.setItem("paketa", paketa);
//     } catch (error) {
//         return null;
//     }
// };
//
// export function getPaketa(){
//     const paketa = localStorage.getItem("paketa");
//     return parseInt(paketa);
// };
