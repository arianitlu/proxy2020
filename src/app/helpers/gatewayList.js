
export function gatewayList() {
    return [
        {
            "id": "96",
            "name": "Afghanistan",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2028",
                "port_end": "2028",
                "range": 1
            },
            "proxy":"af.rareproxy.io",
            "sticky": null
        },
        {
            "id": "112",
            "name": "Albania",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2044",
                "port_end": "2044",
                "range": 1
            },
            "proxy":"al.rareproxy.io",
            "sticky": null
        },
        {
            "id": "43",
            "name": "Argentina",
            "addr": "178.63.131.100",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"ar.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "115",
            "name": "Armenia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2057",
                "port_end": "2057",
                "range": 1
            },
            "proxy":"am.rareproxy.io",
            "sticky": null
        },
        {
            "id": "23",
            "name": "Australia",
            "addr": "94.130.154.110",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"au.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "114",
            "name": "Austria",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2056",
                "port_end": "2056",
                "range": 1
            },
            "proxy":"at.rareproxy.io",
            "sticky": null
        },
        {
            "id": "38",
            "name": "Azerbaijan",
            "addr": "178.63.131.99",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"az.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "138",
            "name": "Bahamas",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2080",
                "port_end": "2080",
                "range": 1
            },
            "proxy":"bs.rareproxy.io",
            "sticky": null
        },
        {
            "id": "97",
            "name": "Bahrain",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2029",
                "port_end": "2029",
                "range": 1
            },
            "proxy":"bh.rareproxy.io",
            "sticky": null
        },
        {
            "id": "98",
            "name": "Bangladesh",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2030",
                "port_end": "2030",
                "range": 1
            },
            "proxy":"bd.rareproxy.io",
            "sticky": null
        },
        {
            "id": "118",
            "name": "Belarus",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2060",
                "port_end": "2060",
                "range": 1
            },
            "proxy":"by.rareproxy.io",
            "sticky": null
        },
        {
            "id": "17",
            "name": "Belgium",
            "addr": "94.130.154.108",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"be.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "116",
            "name": "Bosnia and Herzegovina\t",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2058",
                "port_end": "2058",
                "range": 1
            },
            "proxy":"ba.rareproxy.io",
            "sticky": null
        },
        {
            "id": "42",
            "name": "Brazil",
            "addr": "178.63.131.100",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"br.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "117",
            "name": "Bulgaria",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2059",
                "port_end": "2059",
                "range": 1
            },
            "proxy":"bg.rareproxy.io",
            "sticky": null
        },
        {
            "id": "101",
            "name": "Cambodia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2033",
                "port_end": "2033",
                "range": 1
            },
            "proxy":"kh.rareproxy.io",
            "sticky": null
        },
        {
            "id": "70",
            "name": "Cameroon",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2003",
                "port_end": "2003",
                "range": 1
            },
            "proxy":"cm.rareproxy.io",
            "sticky": null
        },
        {
            "id": "7",
            "name": "Canada",
            "addr": "94.130.207.203",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"ca.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "44",
            "name": "Chile",
            "addr": "178.63.131.101",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"cl.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "31",
            "name": "China",
            "addr": "178.63.131.98",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"cn.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "47",
            "name": "Colombia",
            "addr": "178.63.131.101",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"co.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "141",
            "name": "Costa Rica",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2083",
                "port_end": "2083",
                "range": 1
            },
            "proxy":"cr.rareproxy.io",
            "sticky": null
        },
        {
            "id": "119",
            "name": "Croatia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2061",
                "port_end": "2061",
                "range": 1
            },
            "proxy":"hr.rareproxy.io",
            "sticky": null
        },
        {
            "id": "142",
            "name": "Cuba",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2084",
                "port_end": "2084",
                "range": 1
            },
            "proxy":"cu.rareproxy.io",
            "sticky": null
        },
        {
            "id": "120",
            "name": "Cyprus",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2062",
                "port_end": "2062",
                "range": 1
            },
            "proxy":"cy.rareproxy.io",
            "sticky": null
        },
        {
            "id": "121",
            "name": "Czech Republic\t",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2063",
                "port_end": "2063",
                "range": 1
            },
            "proxy":"cz.rareproxy.io",
            "sticky": null
        },
        {
            "id": "52",
            "name": "Denmark",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "23000",
                "port_end": "23000",
                "range": 1
            },
            "proxy":"dk.rareproxy.io",
            "sticky": {
                "port_start": "23001",
                "port_end": "23999",
                "range": 998
            }
        },
        {
            "id": "46",
            "name": "Ecuador",
            "addr": "178.63.131.101",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"ec.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "50",
            "name": "Egypt",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"eg.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "122",
            "name": "Estonia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2064",
                "port_end": "2064",
                "range": 1
            },
            "proxy":"ee.rareproxy.io",
            "sticky": null
        },
        {
            "id": "74",
            "name": "Ethiopia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2007",
                "port_end": "2007",
                "range": 1
            },
            "proxy":"et.rareproxy.io",
            "sticky": null
        },
        {
            "id": "53",
            "name": "Finland",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "28000",
                "port_end": "28000",
                "range": 1
            },
            "proxy":"fi.rareproxy.io",
            "sticky": {
                "port_start": "28001",
                "port_end": "28999",
                "range": 998
            }
        },
        {
            "id": "10",
            "name": "France",
            "addr": "94.130.207.217",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"fr.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "123",
            "name": "Georgia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2065",
                "port_end": "2065",
                "range": 1
            },
            "proxy":"ge.rareproxy.io",
            "sticky": null
        },
        {
            "id": "9",
            "name": "Germany",
            "addr": "94.130.207.217",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"de.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            },
        },
        {
            "id": "77",
            "name": "Ghana",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2010",
                "port_end": "2010",
                "range": 1
            },
            "proxy":"gh.rareproxy.io",
            "sticky": null
        },
        {
            "id": "14",
            "name": "Greece",
            "addr": "94.130.207.226",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"gr.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "68",
            "name": "Hong Kong\t",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2001",
                "port_end": "2001",
                "range": 1
            },
            "proxy":"hk.rareproxy.io",
            "sticky": null
        },
        {
            "id": "124",
            "name": "Hungary",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2066",
                "port_end": "2066",
                "range": 1
            },
            "proxy":"hk.rareproxy.io",
            "sticky": null
        },
        {
            "id": "34",
            "name": "India",
            "addr": "178.63.131.98",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"in.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "37",
            "name": "Indonesia",
            "addr": "178.63.131.99",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"id.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "36",
            "name": "Iran",
            "addr": "178.63.131.99",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"ir.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "102",
            "name": "Iraq",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2034",
                "port_end": "2034",
                "range": 1
            },
            "proxy":"iq.rareproxy.io",
            "sticky": null
        },
        {
            "id": "126",
            "name": "Ireland",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2068",
                "port_end": "2068",
                "range": 1
            },
            "proxy":"ie.rareproxy.io",
            "sticky": null
        },
        {
            "id": "21",
            "name": "Israel",
            "addr": "94.130.154.109",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"il.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "12",
            "name": "Italy",
            "addr": "94.130.207.226",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"it.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "146",
            "name": "Jamaica",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2088",
                "port_end": "2088",
                "range": 1
            },
            "proxy":"jm.rareproxy.io",
            "sticky": null
        },
        {
            "id": "28",
            "name": "Japan",
            "addr": "178.63.131.97",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"jp.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "39",
            "name": "Kazakhstan",
            "addr": "178.63.131.99",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"kz.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "78",
            "name": "Kenya",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2011",
                "port_end": "2011",
                "range": 1
            },
            "proxy":"ke.rareproxy.io",
            "sticky": null
        },
        {
            "id": "127",
            "name": "Latvia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2069",
                "port_end": "2069",
                "range": 1
            },
            "proxy":"lv.rareproxy.io",
            "sticky": null
        },
        {
            "id": "128",
            "name": "Liechtenstein",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2070",
                "port_end": "2070",
                "range": 1
            },
            "proxy":"li.rareproxy.io",
            "sticky": null
        },
        {
            "id": "129",
            "name": "Lithuania",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2071",
                "port_end": "2071",
                "range": 1
            },
            "proxy":"lt.rareproxy.io",
            "sticky": null
        },
        {
            "id": "130",
            "name": "Luxembourg",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2072",
                "port_end": "2072",
                "range": 1
            },
            "proxy":"lu.rareproxy.io",
            "sticky": null
        },
        {
            "id": "137",
            "name": "Macedonia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2079",
                "port_end": "2079",
                "range": 1
            },
            "proxy":"mk.rareproxy.io",
            "sticky": null
        },
        {
            "id": "80",
            "name": "Madagascar",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2013",
                "port_end": "2013",
                "range": 1
            },
            "proxy":"mg.rareproxy.io",
            "sticky": null
        },
        {
            "id": "24",
            "name": "Malaysia",
            "addr": "94.130.154.110",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"my.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "83",
            "name": "Mauritius",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2016",
                "port_end": "2016",
                "range": 1
            },
            "proxy":"mu.rareproxy.io",
            "sticky": null
        },
        {
            "id": "41",
            "name": "Mexico",
            "addr": "178.63.131.100",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"mx.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "132",
            "name": "Moldova",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2074",
                "port_end": "2074",
                "range": 1
            },
            "proxy":"md.rareproxy.io",
            "sticky": null
        },
        {
            "id": "106",
            "name": "Mongolia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2038",
                "port_end": "2038",
                "range": 1
            },
            "proxy":"mn.rareproxy.io",
            "sticky": null
        },
        {
            "id": "133",
            "name": "Montenegro",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2075",
                "port_end": "2075",
                "range": 1
            },
            "proxy":"mn.rareproxy.io",
            "sticky": null
        },
        {
            "id": "84",
            "name": "Morocco",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2017",
                "port_end": "2017",
                "range": 1
            },
            "proxy":"ma.rareproxy.io",
            "sticky": null
        },
        {
            "id": "85",
            "name": "Mozambique",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2018",
                "port_end": "2018",
                "range": 1
            },
            "proxy":"mz.rareproxy.io",
            "sticky": null
        },
        {
            "id": "100",
            "name": "Myanmar",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2032",
                "port_end": "2032",
                "range": 1
            },
            "proxy":"mm.rareproxy.io",
            "sticky": null
        },
        {
            "id": "16",
            "name": "Netherlands",
            "addr": "94.130.154.108",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"nl.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "152",
            "name": "New Zealand",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2094",
                "port_end": "2094",
                "range": 1
            },
            "proxy":"nz.rareproxy.io",
            "sticky": null
        },
        {
            "id": "86",
            "name": "Nigeria",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2019",
                "port_end": "2019",
                "range": 1
            },
            "proxy":"ng.rareproxy.io",
            "sticky": null
        },
        {
            "id": "54",
            "name": "Norway",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "42000",
                "port_end": "42000",
                "range": 1
            },
            "proxy":"no.rareproxy.io",
            "sticky": {
                "port_start": "42001",
                "port_end": "42999",
                "range": 998
            }
        },
        {
            "id": "107",
            "name": "Oman",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2039",
                "port_end": "2039",
                "range": 1
            },
            "proxy":"om.rareproxy.io",
            "sticky": null
        },
        {
            "id": "35",
            "name": "Pakistan",
            "addr": "178.63.131.98",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"pk.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "148",
            "name": "Panama",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2090",
                "port_end": "2090",
                "range": 1
            },
            "proxy":"pa.rareproxy.io",
            "sticky": null
        },
        {
            "id": "154",
            "name": "Paraguay",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2096",
                "port_end": "2096",
                "range": 1
            },
            "proxy":"py.rareproxy.io",
            "sticky": null
        },
        {
            "id": "45",
            "name": "Peru",
            "addr": "178.63.131.101",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"pe.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "29",
            "name": "Philippines",
            "addr": "178.63.131.97",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"ph.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "20",
            "name": "Poland",
            "addr": "94.130.154.109",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"pl.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "15",
            "name": "Portugal",
            "addr": "94.130.154.108",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"pt.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "149",
            "name": "Puerto Rico",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2091",
                "port_end": "2091",
                "range": 1
            },
            "proxy":"pr.rareproxy.io",
            "sticky": null
        },
        {
            "id": "108",
            "name": "Qatar",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2040",
                "port_end": "2040",
                "range": 1
            },
            "proxy":"qa.rareproxy.io",
            "sticky": null
        },
        {
            "id": "55",
            "name": "Romania",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "43000",
                "port_end": "43000",
                "range": 1
            },
            "proxy":"ro.rareproxy.io",
            "sticky": {
                "port_start": "43001",
                "port_end": "43999",
                "range": 998
            }
        },
        {
            "id": "18",
            "name": "Russia",
            "addr": "94.130.154.108",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"ru.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "51",
            "name": "Saudi Arabia",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"sa.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "22999",
                "range": 998
            }
        },
        {
            "id": "87",
            "name": "Senegal",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2020",
                "port_end": "2020",
                "range": 1
            },
            "proxy":"sn.rareproxy.io",
            "sticky": null
        },
        {
            "id": "134",
            "name": "Serbia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2076",
                "port_end": "2076",
                "range": 1
            },
            "proxy":"rs.rareproxy.io",
            "sticky": null
        },
        {
            "id": "88",
            "name": "Seychelles",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2021",
                "port_end": "2021",
                "range": 1
            },
            "proxy":"sc.rareproxy.io",
            "sticky": null
        },
        {
            "id": "30",
            "name": "Singapore",
            "addr": "178.63.131.97",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"sg.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "135",
            "name": "Slovakia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2077",
                "port_end": "2077",
                "range": 1
            },
            "proxy":"sk.rareproxy.io",
            "sticky": null
        },
        {
            "id": "136",
            "name": "Slovenia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2078",
                "port_end": "2078",
                "range": 1
            },
            "proxy":"si.rareproxy.io",
            "sticky": null
        },
        {
            "id": "49",
            "name": "South Africa",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"za.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "27",
            "name": "South Korea",
            "addr": "94.130.154.110",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"kr.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "11",
            "name": "Spain",
            "addr": "94.130.207.226",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"es.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "91",
            "name": "Sudan",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2024",
                "port_end": "2024",
                "range": 1
            },
            "proxy":"sd.rareproxy.io",
            "sticky": null
        },
        {
            "id": "13",
            "name": "Sweden",
            "addr": "94.130.207.226",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"se.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "56",
            "name": "Switzerland",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "44000",
                "port_end": "44000",
                "range": 1
            },
            "proxy":"ch.rareproxy.io",
            "sticky": {
                "port_start": "44001",
                "port_end": "44999",
                "range": 998
            }
        },
        {
            "id": "57",
            "name": "Syria",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "45000",
                "port_end": "45000",
                "range": 1
            },
            "proxy":"sy.rareproxy.io",
            "sticky": {
                "port_start": "45001",
                "port_end": "45999",
                "range": 998
            }
        },
        {
            "id": "33",
            "name": "Taiwan",
            "addr": "178.63.131.98",
            "rotating": {
                "port_start": "12000",
                "port_end": "12000",
                "range": 1
            },
            "proxy":"tw.rareproxy.io",
            "sticky": {
                "port_start": "12001",
                "port_end": "21999",
                "range": 9998
            }
        },
        {
            "id": "26",
            "name": "Thailand",
            "addr": "94.130.154.110",
            "rotating": {
                "port_start": "22000",
                "port_end": "22000",
                "range": 1
            },
            "proxy":"th.rareproxy.io",
            "sticky": {
                "port_start": "22001",
                "port_end": "31999",
                "range": 9998
            }
        },
        {
            "id": "150",
            "name": "Trinidad and Tobago",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2092",
                "port_end": "2092",
                "range": 1
            },
            "proxy":"tt.rareproxy.io",
            "sticky": null
        },
        {
            "id": "93",
            "name": "Tunisia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2025",
                "port_end": "2025",
                "range": 1
            },
            "proxy":"tn.rareproxy.io",
            "sticky": null
        },
        {
            "id": "22",
            "name": "Turkey",
            "addr": "94.130.154.109",
            "rotating": {
                "port_start": "32000",
                "port_end": "32000",
                "range": 1
            },
            "proxy":"tr.rareproxy.io",
            "sticky": {
                "port_start": "32001",
                "port_end": "41999",
                "range": 9998
            }
        },
        {
            "id": "94",
            "name": "Uganda",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2026",
                "port_end": "2026",
                "range": 1
            },
            "proxy":"ug.rareproxy.io",
            "sticky": null
        },
        {
            "id": "19",
            "name": "Ukraine",
            "addr": "94.130.154.109",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"ua.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "40",
            "name": "United Arab Emirates",
            "addr": "178.63.131.100",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"ae.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "11999",
                "range": 9998
            }
        },
        {
            "id": "8",
            "name": "United Kingdom",
            "addr": "94.130.207.217",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"uk.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "21999",
                "range": 19998
            }
        },
        {
            "id": "1",
            "name": "United States",
            "addr": "94.130.207.203",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"us.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "21999",
                "range": 19998
            }
        },
        {
            "id": "110",
            "name": "Uzbekistan",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2042",
                "port_end": "2042",
                "range": 1
            },
            "proxy":"uz.rareproxy.io",
            "sticky": null
        },
        {
            "id": "66",
            "name": "Vietnam",
            "addr": "178.63.131.102",
            "rotating": {
                "port_start": "46000",
                "port_end": "46000",
                "range": 1
            },
            "proxy":"vn.rareproxy.io",
            "sticky": {
                "port_start": "46001",
                "port_end": "46001",
                "range": 0
            }
        },
        {
            "id": "157",
            "name": "Worldwide",
            "addr": "178.63.131.105",
            "rotating": {
                "port_start": "2000",
                "port_end": "2000",
                "range": 1
            },
            "proxy":"world.rareproxy.io",
            "sticky": {
                "port_start": "2001",
                "port_end": "42000",
                "range": 39999
            }
        },
        {
            "id": "95",
            "name": "Zambia",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2027",
                "port_end": "2027",
                "range": 1
            },
            "proxy":"zm.rareproxy.io",
            "sticky": null
        },
        {
            "id": "89",
            "name": "Zimbabwe",
            "addr": "178.63.131.103",
            "rotating": {
                "port_start": "2022",
                "port_end": "2022",
                "range": 1
            },
            "proxy":"zw.rareproxy.io",
            "sticky": null
        }
    ]
}