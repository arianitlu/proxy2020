export function requestHeaders() {
    return {
        Accept: "application/json",
        "X-API-KEY":"5203dda1-b908-4cc6-b8f2-ea73dabecb21",
        "Content-Type":"application/json",
        "Access-Control-Allow-Headers":
            " Origin, Content-Type, X-Auth-Token,X-Requested-With,Content-Type, Accept",
        "Access-Control-Allow-Credentials": "true",
        "X-Requested-With": "XMLHttpRequest"
    };
}
//
//
// res.header("Access-Control-Allow-Origin", "*");
// res.header("Access-Control-Allow-Headers", "X-Requested-With");

export function requestCoinbaseHeaders() {
    return {
        Accept: "application/json",
        "X-CC-Api-Key":"cb9252fc-deaf-409a-aef1-81d2d529d088",
        "Content-Type":"application/json"
    };
    // cb9252fc-deaf-409a-aef1-81d2d529d088
}

export function requestHeadersOurDb() {
    return {
        Accept: "application/json",
        "X-API-KEY":"cb9252fc-deaf-409a-aef1-81d2d529d088",
        "Content-Type":"application/json"
    };
}